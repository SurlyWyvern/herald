﻿using System;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Interface for serialization contracts.
	/// </summary>
	public interface IContract {
		#region Methods

		/// <summary>
		/// Returns true if this instance can process the given type.
		/// </summary>
		bool CanProcess(Type type);
		
		/// <summary>
		/// Serialize the given value using the given serializer.
		/// </summary>
		void Write(Serializer serializer, object value);

		/// <summary>
		/// Deserialize a value from the given Deserializer.
		/// </summary>
		/// <param name="type">The type to be deserialized.</param>
		/// <param name="existingValue">For reference types, provides a newly-created instance to populate.</param>
		object Read(Deserializer deserializer, Type type = null, object existingValue = null);

		#endregion
	}
}
