﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Serialization binder that associates types with string 'aliases'. These
	/// aliases are either the assembly-qualified type name, or can be defined
	/// for specific types at initialization.
	/// </summary>
	public class AliasBinder : SerializationBinder {
		#region Properties

		/// <summary>
		/// Default instance that uses the default settings.
		/// </summary>
		public static AliasBinder DefaultInstance {
			get;
		}

		/// <summary>
		/// If true, version, culture, and public key token data will be used in
		/// in aliases.
		/// </summary>
		public bool Exact {
			get;
		}

		/// <summary>
		/// Whether to use the default aliases for primitives and builtin types.
		/// </summary>
		public bool UseDefaults {
			get;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the type defined by the given assembly and type name.
		/// </summary>
		public override Type BindToType(string assemblyName, string typeName) {
			var fullAssemblyName = new AssemblyName(assemblyName);
			assemblyName = Exact ? fullAssemblyName.FullName : fullAssemblyName.Name;

			var alias = typeName + "," + assemblyName;
			if(UseDefaults && defaultAliasToType.TryGetValue(alias, out Type result))
				return result;

			if(aliasToType.TryGetValue(alias, out result))
				return result;
			
			var assembly = Assembly.Load(fullAssemblyName);
			if(null == assembly)
				return null;

			result = assembly.GetType(typeName);
			if(null == result)
				return null;

			RegisterAlias(alias, result);
			return result;
		}

		/// <summary>
		/// Gets the type associated with the given alias.
		/// </summary>
		public Type BindToType(string alias) {
			if(UseDefaults && defaultAliasToType.TryGetValue(alias, out Type result))
				return result;

			if(aliasToType.TryGetValue(alias, out result))
				return result;

			// Split into type name and assembly name
			int splitAt = alias.IndexOf(',');
			if(splitAt < 0)
				return null;
			return BindToType(alias.Substring(splitAt + 1),
				alias.Substring(0, splitAt));
		}

		/// <summary>
		/// Gets the alias associated with the given type.
		/// </summary>
		public string BindToAlias(Type type) {
			if(typeToAlias.TryGetValue(type, out string result))
				return result;

			if(UseDefaults && defaultTypeToAlias.TryGetValue(type, out result))
				return result;

			var assemblyName = type.Assembly.GetName();

			string alias;
			// For generic types, we need to check for pre-registered aliases
			// for the generic arguments.
			if(type.IsGenericType) {
				var builder = new StringBuilder(type.Namespace + "." + type.Name + "[");
				foreach(var genarg in type.GetGenericArguments()) {
					builder.Append("[");
					builder.Append(BindToAlias(genarg));
					builder.Append("]");
				}
				builder.Append("],");
				builder.Append(Exact ? assemblyName.FullName : assemblyName.Name);
				alias = builder.ToString();
			} else {
				alias = type.FullName + ", " + (Exact ? assemblyName.FullName : assemblyName.Name);
			}
			RegisterAlias(alias, type);
			return alias;
		}

		private bool RegisterAlias(string alias, Type type) {
			if(null == alias || null == type)
				return false;
			if(aliasToType.ContainsKey(alias) || typeToAlias.ContainsKey(type))
				return false;
			aliasToType.Add(alias, type);
			typeToAlias.Add(type, alias);
			return true;
		}
		
		#endregion
		
		#region Constructors

		static AliasBinder() {
			void BindDefault(string a, Type t) {
				defaultAliasToType.Add(a, t);
				defaultTypeToAlias.Add(t, a);
			}

			defaultAliasToType = new Dictionary<string, Type>();
			defaultTypeToAlias = new Dictionary<Type, string>();

			BindDefault("bool", typeof(bool));
			BindDefault("sbyte", typeof(sbyte));
			BindDefault("short", typeof(short));
			BindDefault("int", typeof(int));
			BindDefault("long", typeof(long));
			BindDefault("byte", typeof(byte));
			BindDefault("ushort", typeof(ushort));
			BindDefault("uint", typeof(uint));
			BindDefault("ulong", typeof(ulong));
			BindDefault("char", typeof(char));
			BindDefault("float", typeof(float));
			BindDefault("double", typeof(double));
			BindDefault("decimal", typeof(decimal));
			BindDefault("DateTime", typeof(DateTime));
			BindDefault("Guid", typeof(Guid));
			BindDefault("string", typeof(string));

			DefaultInstance = new AliasBinder();
		}

		/// <summary>
		/// Default ctor.
		/// </summary>
		public AliasBinder() {
			Exact = true;
			UseDefaults = true;
			aliasToType = new Dictionary<string, Type>();
			typeToAlias = new Dictionary<Type, string>();
		}

		/// <summary>
		/// Constructs an instance, priming it with the given alias-Type pairs.
		/// </summary>
		public AliasBinder(params KeyValuePair<string, Type>[] aliases) {
			Exact = true;
			UseDefaults = true;
			aliasToType = new Dictionary<string, Type>(aliases.Length);
			typeToAlias = new Dictionary<Type, string>(aliases.Length);
			foreach(var entry in aliases)
				RegisterAlias(entry.Key, entry.Value);
		}

		/// <summary>
		/// Constructs an instance with the given options.
		/// </summary>
		public AliasBinder(bool exact, bool useDefaults, params KeyValuePair<string, Type>[] aliases) {
			Exact = exact;
			UseDefaults = useDefaults;
			aliasToType = new Dictionary<string, Type>(aliases.Length);
			typeToAlias = new Dictionary<Type, string>(aliases.Length);
			foreach(var entry in aliases)
				RegisterAlias(entry.Key, entry.Value);
		}
		
		#endregion

		#region Fields

		private readonly Dictionary<string, Type> aliasToType;
		private readonly Dictionary<Type, string> typeToAlias;

		private readonly static Dictionary<string, Type> defaultAliasToType;
		private readonly static Dictionary<Type, string> defaultTypeToAlias;

		#endregion
	}
}
