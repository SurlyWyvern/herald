﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Provides easy access to reflected serialization and deserialization methods.
	/// </summary>
	public class Reflection {
		#region Serialization

		public readonly static MethodInfo Serialize;
		public readonly static MethodInfo SerializeSelf;
		public readonly static MethodInfo SerializeExplicit;
		public readonly static MethodInfo SerializeExplicitSelf;
		public readonly static MethodInfo SerializeExplicitType;
		public readonly static MethodInfo SerializeExplicitString;
		public readonly static MethodInfo SerializeNullable;
		public readonly static MethodInfo SerializePoly;

		public readonly static MethodInfo WriteBool;
		public readonly static MethodInfo WriteByte;
		public readonly static MethodInfo WriteUInt16;
		public readonly static MethodInfo WriteUInt32;
		public readonly static MethodInfo WriteUInt64;
		public readonly static MethodInfo WriteSByte;
		public readonly static MethodInfo WriteInt16;
		public readonly static MethodInfo WriteInt32;
		public readonly static MethodInfo WriteInt64;
		public readonly static MethodInfo WriteSingle;
		public readonly static MethodInfo WriteDouble;
		public readonly static MethodInfo WriteChar;
		public readonly static MethodInfo WriteDecimal;
		public readonly static MethodInfo WriteDateTime;
		public readonly static MethodInfo WriteTimeSpan;
		public readonly static MethodInfo WriteDateTimeOffset;
		public readonly static MethodInfo WriteGuid;

		public readonly static MethodInfo WriteNBool;
		public readonly static MethodInfo WriteNByte;
		public readonly static MethodInfo WriteNUInt16;
		public readonly static MethodInfo WriteNUInt32;
		public readonly static MethodInfo WriteNUInt64;
		public readonly static MethodInfo WriteNSByte;
		public readonly static MethodInfo WriteNInt16;
		public readonly static MethodInfo WriteNInt32;
		public readonly static MethodInfo WriteNInt64;
		public readonly static MethodInfo WriteNSingle;
		public readonly static MethodInfo WriteNDouble;
		public readonly static MethodInfo WriteNChar;
		public readonly static MethodInfo WriteNDecimal;
		public readonly static MethodInfo WriteNDateTime;
		public readonly static MethodInfo WriteNTimeSpan;
		public readonly static MethodInfo WriteNDateTimeOffset;
		public readonly static MethodInfo WriteNGuid;

		/// <summary>
		/// Check if the given type can be written with a primitive or
		/// built-in struct Write method.
		/// </summary>
		public static bool TryGetWriteMethod(Type type, out MethodInfo method) {
			return nativeWriteMethods.TryGetValue(type, out method);
		}

		/// <summary>
		/// Emit sufficient IL code to serialize a value of the given type, assuming that
		/// a reference to a Serializer, and the value have been pushed to the stack.
		/// </summary>
		public static void EmitSerialize(Type type, ILGenerator generator) {
			if(type.IsValueType) {
				Type underlyingNullableType;
				if(TryGetWriteMethod(type, out MethodInfo writeMethod)) {
					generator.Emit(OpCodes.Call, writeMethod);
				} else if(null != (underlyingNullableType = Nullable.GetUnderlyingType(type))) {
					var nullableSerialize = SerializeNullable.MakeGenericMethod(underlyingNullableType);
					generator.Emit(OpCodes.Call, nullableSerialize);
				} else {
					generator.Emit(OpCodes.Box, type);
					if(typeof(ISelfContract).IsAssignableFrom(type)) {
						generator.Emit(OpCodes.Castclass, typeof(ISelfContract));
						generator.Emit(OpCodes.Call, SerializeSelf);
					} else {
						generator.Emit(OpCodes.Call, Serialize);
					}
				}
			} else if(type == typeof(string)) {
				generator.Emit(OpCodes.Call, SerializeExplicitString);
			} else if(typeof(Type).IsAssignableFrom(type)) {
				generator.Emit(OpCodes.Call, SerializeExplicitType);
			} else if(type.IsSealed) {
				if(typeof(ISelfContract).IsAssignableFrom(type)) {
					generator.Emit(OpCodes.Castclass, typeof(ISelfContract));
					generator.Emit(OpCodes.Call, SerializeSelf);
				} else {
					generator.Emit(OpCodes.Call, Serialize);
				}
			} else {
				generator.Emit(OpCodes.Ldtoken, type);
				generator.Emit(OpCodes.Call, TypeHandleToType);
				generator.Emit(OpCodes.Call, SerializePoly);
			}
		}

		private readonly static Dictionary<Type, MethodInfo> nativeWriteMethods;

		#endregion

		#region Deserialization

		public static readonly MethodInfo DeserializeImplicit;
		public static readonly MethodInfo Deserialize;
		public static readonly MethodInfo DeserializePoly;
		public static readonly MethodInfo TypeHandleToType;

		public static readonly MethodInfo ReadBool;
		public static readonly MethodInfo ReadByte;
		public static readonly MethodInfo ReadUInt16;
		public static readonly MethodInfo ReadUInt32;
		public static readonly MethodInfo ReadUInt64;
		public static readonly MethodInfo ReadSByte;
		public static readonly MethodInfo ReadInt16;
		public static readonly MethodInfo ReadInt32;
		public static readonly MethodInfo ReadInt64;
		public static readonly MethodInfo ReadSingle;
		public static readonly MethodInfo ReadDouble;
		public static readonly MethodInfo ReadChar;
		public static readonly MethodInfo ReadDecimal;
		public static readonly MethodInfo ReadDateTime;
		public static readonly MethodInfo ReadTimeSpan;
		public static readonly MethodInfo ReadDateTimeOffset;
		public static readonly MethodInfo ReadGuid;

		/// <summary>
		/// Check whether the given type has a defined Deserializer.Read method.
		/// </summary>
		public static bool TryGetReadMethod(Type type, out MethodInfo method) {
			return nativeReadMethods.TryGetValue(type, out method);
		}

		/// <summary>
		/// Emit IL code to deserialize a value of the given type, assuming that
		/// a reference to a <see cref="Deserializer"/> instance is on the stack.
		/// </summary>
		public static void EmitDeserialize(Type type, ILGenerator generator) {
			if(null == type)
				throw new ArgumentNullException("type");
			if(null == generator)
				throw new ArgumentNullException("generator");

			if(type.IsValueType) {
				if(TryGetReadMethod(type, out MethodInfo readMethod)) {
					generator.Emit(OpCodes.Call, readMethod);
				} else {
					generator.Emit(OpCodes.Ldtoken, type);
					generator.Emit(OpCodes.Call, TypeHandleToType);
					generator.Emit(OpCodes.Call, Deserialize);
					generator.Emit(OpCodes.Unbox_Any, type);
				}
			} else if(type == typeof(string) || typeof(Type).IsAssignableFrom(type)) {
				generator.Emit(OpCodes.Call, DeserializeImplicit);
				generator.Emit(OpCodes.Castclass, type);
			} else if(type.IsSealed) {
				generator.Emit(OpCodes.Ldtoken, type);
				generator.Emit(OpCodes.Call, TypeHandleToType);
				generator.Emit(OpCodes.Call, Deserialize);
				generator.Emit(OpCodes.Castclass, type);
			} else {
				generator.Emit(OpCodes.Ldtoken, type);
				generator.Emit(OpCodes.Call, TypeHandleToType);
				generator.Emit(OpCodes.Call, DeserializePoly);
				generator.Emit(OpCodes.Castclass, type);
			}
		}

		private readonly static Dictionary<Type, MethodInfo> nativeReadMethods;

		#endregion

		static Reflection() {
			var serializerType = typeof(Serializer);
			Serialize = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(object) });
			SerializeSelf = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(ISelfContract) });
			SerializeExplicit = serializerType.GetMethod(nameof(Serializer.SerializeExplicit), new Type[] { typeof(object) });
			SerializeExplicitSelf = serializerType.GetMethod(nameof(Serializer.SerializeExplicit), new Type[] { typeof(ISelfContract) });
			SerializeExplicitType = serializerType.GetMethod(nameof(Serializer.SerializeExplicit), new Type[] { typeof(Type) });
			SerializeExplicitString = serializerType.GetMethod(nameof(Serializer.SerializeExplicit), new Type[] { typeof(string) });
			SerializeNullable = serializerType.GetMethods().Single(f => f.Name == nameof(Serializer.Serialize) && f.IsGenericMethodDefinition);
			SerializePoly = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(object), typeof(Type) });
			
			WriteBool = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(bool) });
				WriteByte = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(byte) });
				WriteUInt16 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(ushort) });
				WriteUInt32 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(uint) });
				WriteUInt64 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(ulong) });
				WriteSByte = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(sbyte) });
				WriteInt16 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(short) });
				WriteInt32 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(int) });
				WriteInt64 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(long) });
				WriteSingle = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(float) });
				WriteDouble = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(double) });
				WriteChar = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(char) });
				WriteDecimal = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(decimal) });
				WriteDateTime = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(DateTime) });
				WriteTimeSpan = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(TimeSpan) });
				WriteDateTimeOffset = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(DateTimeOffset) });
				WriteGuid = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(Guid) });

				WriteNBool = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(bool?) });
				WriteNByte = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(byte?) });
				WriteNUInt16 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(ushort?) });
				WriteNUInt32 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(uint?) });
				WriteNUInt64 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(ulong?) });
				WriteNSByte = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(sbyte?) });
				WriteNInt16 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(short?) });
				WriteNInt32 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(int?) });
				WriteNInt64 = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(long?) });
				WriteNSingle = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(float?) });
				WriteNDouble = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(double?) });
				WriteNChar = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(char?) });
				WriteNDecimal = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(decimal?) });
				WriteNDateTime = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(DateTime?) });
				WriteNTimeSpan = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(TimeSpan?) });
				WriteNDateTimeOffset = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(DateTimeOffset?) });
				WriteNGuid = serializerType.GetMethod(nameof(Serializer.Serialize), new Type[] { typeof(Guid?) });

			nativeWriteMethods = new Dictionary<Type, MethodInfo>() {
				[typeof(bool)] = WriteBool,
				[typeof(byte)] = WriteByte,
				[typeof(ushort)] = WriteUInt16,
				[typeof(uint)] = WriteUInt32,
				[typeof(ulong)] = WriteUInt64,
				[typeof(sbyte)] = WriteSByte,
				[typeof(short)] = WriteInt16,
				[typeof(int)] = WriteInt32,
				[typeof(long)] = WriteInt64,
				[typeof(float)] = WriteSingle,
				[typeof(double)] = WriteDouble,
				[typeof(char)] = WriteChar,
				[typeof(decimal)] = WriteDecimal,
				[typeof(DateTime)] = WriteDateTime,
				[typeof(TimeSpan)] = WriteTimeSpan,
				[typeof(DateTimeOffset)] = WriteDateTimeOffset,
				[typeof(Guid)] = WriteGuid,

				[typeof(bool?)] = WriteNBool,
				[typeof(byte?)] = WriteNByte,
				[typeof(ushort?)] = WriteNUInt16,
				[typeof(uint?)] = WriteNUInt32,
				[typeof(ulong?)] = WriteNUInt64,
				[typeof(sbyte?)] = WriteNSByte,
				[typeof(short?)] = WriteNInt16,
				[typeof(int?)] = WriteNInt32,
				[typeof(long?)] = WriteNInt64,
				[typeof(float?)] = WriteNSingle,
				[typeof(double?)] = WriteNDouble,
				[typeof(char?)] = WriteNChar,
				[typeof(decimal?)] = WriteNDecimal,
				[typeof(DateTime?)] = WriteNDateTime,
				[typeof(TimeSpan?)] = WriteNTimeSpan,
				[typeof(DateTimeOffset?)] = WriteNDateTimeOffset,
				[typeof(Guid?)] = WriteNGuid,

			};

			var deserializerType = typeof(Deserializer);

			DeserializeImplicit = deserializerType.GetMethods().Single(m => m.Name == nameof(Deserializer.Deserialize) && m.GetParameters().Length == 0 && !m.IsGenericMethodDefinition);
			Deserialize = deserializerType.GetMethod(nameof(Deserializer.Deserialize), new Type[] { typeof(Type) });
			DeserializePoly = deserializerType.GetMethod(nameof(Deserializer.DeserializePoly), new Type[] { typeof(Type) });
			TypeHandleToType = typeof(Type).GetMethod(nameof(Type.GetTypeFromHandle));

			ReadBool = deserializerType.GetMethod(nameof(Deserializer.ReadBoolean));
			ReadByte = deserializerType.GetMethod(nameof(Deserializer.ReadByte));
			ReadUInt16 = deserializerType.GetMethod(nameof(Deserializer.ReadUInt16));
			ReadUInt32 = deserializerType.GetMethod(nameof(Deserializer.ReadUInt32));
			ReadUInt64 = deserializerType.GetMethod(nameof(Deserializer.ReadUInt64));
			ReadSByte = deserializerType.GetMethod(nameof(Deserializer.ReadSByte));
			ReadInt16 = deserializerType.GetMethod(nameof(Deserializer.ReadInt16));
			ReadInt32 = deserializerType.GetMethod(nameof(Deserializer.ReadInt32));
			ReadInt64 = deserializerType.GetMethod(nameof(Deserializer.ReadInt64));
			ReadSingle = deserializerType.GetMethod(nameof(Deserializer.ReadSingle));
			ReadDouble = deserializerType.GetMethod(nameof(Deserializer.ReadDouble));
			ReadChar = deserializerType.GetMethod(nameof(Deserializer.ReadChar));
			ReadDecimal = deserializerType.GetMethod(nameof(Deserializer.ReadDecimal));
			ReadDateTime = deserializerType.GetMethod(nameof(Deserializer.ReadDateTime));
			ReadTimeSpan = deserializerType.GetMethod(nameof(Deserializer.ReadTimeSpan));
			ReadDateTimeOffset = deserializerType.GetMethod(nameof(Deserializer.ReadDateTimeOffset));
			ReadGuid = deserializerType.GetMethod(nameof(Deserializer.ReadGuid));

			nativeReadMethods = new Dictionary<Type, MethodInfo>() {
				[typeof(bool)] = ReadBool,
				[typeof(byte)] = ReadByte,
				[typeof(ushort)] = ReadUInt16,
				[typeof(uint)] = ReadUInt32,
				[typeof(ulong)] = ReadUInt64,
				[typeof(sbyte)] = ReadSByte,
				[typeof(short)] = ReadInt16,
				[typeof(int)] = ReadInt32,
				[typeof(long)] = ReadInt64,
				[typeof(float)] = ReadSingle,
				[typeof(double)] = ReadDouble,
				[typeof(char)] = ReadChar,
				[typeof(decimal)] = ReadDecimal,
				[typeof(DateTime)] = ReadDateTime,
				[typeof(TimeSpan)] = ReadTimeSpan,
				[typeof(DateTimeOffset)] = ReadDateTimeOffset,
				[typeof(Guid)] = ReadGuid,
			};
			
		}
	}
}
