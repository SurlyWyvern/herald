﻿using System;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Interface for contract factories. These allow contracts to be generated
	/// for generic types at runtime.
	/// </summary>
	public interface IContractFactory {
		/// <summary>
		/// Returns true if the factory can create a contract for the given type.
		/// </summary>
		bool CanCreateFor(Type type);

		/// <summary>
		/// Creates a contract for the given type.
		/// </summary>
		IContract CreateFor(Type type);
	}
}
