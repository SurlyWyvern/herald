﻿namespace SurlyWyvern.Herald {
	/// <summary>
	/// Interface to implement for types to control their own serialization.
	/// </summary>
	public interface ISelfContract {
		/// <summary>
		/// Write this instance's data to the given Serializer.
		/// </summary>
		void Write(Serializer serializer);

		/// <summary>
		/// Read this instance's data from the deserializer.
		/// </summary>
		void Read(Deserializer deserializer);
	}
}
