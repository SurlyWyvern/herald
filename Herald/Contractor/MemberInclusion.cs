﻿using System;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Denotes which members of a type are serialized.
	/// </summary>
	/// <remarks>
	/// In most cases, AllFields is adequate; ILContract skips visibility checks
	/// and can access inherited private fields, such as property backing fields.
	/// PublicOnly should be used in cases where visibility checks cannot be bypassed.
	/// </remarks>
	[Flags]
	public enum MemberInclusion {
		None = 0,
		PublicFields = 1,
		NonPublicFields = 2,
		PublicProperties = 4,
		NonPublicProperties = 8,

		AllFields = PublicFields | NonPublicFields,
		PublicOnly = PublicFields | PublicProperties,
	}

}
