﻿using System;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Interface for contract providers.
	/// </summary>
	public interface IContractor {
		/// <summary>
		/// Get a contract that can process the given type.
		/// </summary>
		IContract GetContract(Type type);

		/// <summary>
		/// Get a contract based on token and type. This overload is provided for the
		/// benefit of Arrays, which record the element type, not array type.
		/// </summary>
		IContract GetContract(Token token, Type type);
	}
}
