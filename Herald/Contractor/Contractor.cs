﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using SurlyWyvern.Herald.Extensions;
using SurlyWyvern.Herald.DefaultContracts;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Generates serialization contracts using runtime IL generation.
	/// </summary>
	public class Contractor : IContractor {
		#region Properties
		
		/// <summary>
		/// Determines what fields and properties will be serialized by auto contracts.
		/// </summary>
		public MemberInclusion ContractMode {
			get;
		}
		
		/// <summary>
		/// An instance created with the default constructor.
		/// </summary>
		public static Contractor Default {
			get;
		}
		
		#endregion

		#region Methods

		public IContract GetContract(Type type) {
			if(null == type)
				throw new ArgumentNullException("type");
			if(contractMap.TryGetValue(type, out IContract result))
				return result;
			else if(TryFindCustomContract(type, out result))
				return result;
			else if(arrayContractFactory.CanCreateFor(type)) {
				result = arrayContractFactory.CreateFor(type);
				contractMap.Add(type, result);
				return result;
			} else if(collectionContractFactory.CanCreateFor(type)) {
				result = collectionContractFactory.CreateFor(type);
				contractMap.Add(type, result);
				return result;
			} else if(keyValuePairContractFactory.CanCreateFor(type)) {
				result = keyValuePairContractFactory.CreateFor(type);
				contractMap.Add(type, result);
				return result;
			} else
				return GenerateContract(type);
		}

		public IContract GetContract(Token token, Type type) {
			if(token == Token.Array || token == Token.ArrayRef)
				type = type.MakeArrayType();
			if(contractMap.TryGetValue(type, out IContract result))
				return result;
			else if(TryFindCustomContract(type, out result))
				return result;
			switch(token) {
				case Token.Array:
				case Token.ArrayRef:
					result = arrayContractFactory.CreateFor(type);
					contractMap.Add(type, result);
					return result;
				case Token.Struct:
					if(keyValuePairContractFactory.CanCreateFor(type)) {
						result = keyValuePairContractFactory.CreateFor(type);
						contractMap.Add(type, result);
						return result;
					} else {
						return GenerateContract(type);
					}
				case Token.Object:
				case Token.ObjectRef:
					if(collectionContractFactory.CanCreateFor(type)) {
						result = collectionContractFactory.CreateFor(type);
						contractMap.Add(type, result);
						return result;
					} else {
						return GenerateContract(type);
					}
			}
			return null;
		}

		private bool TryFindCustomContract(Type type, out IContract result) {
			int i;
			// Check customContracts.
			for(i = 0; i < customContracts.Length; ++i) {
				result = customContracts[i];
				if(result.CanProcess(type)) {
					contractMap.Add(type, result);
					return true;
				}
			}
			// Check custom factories.
			for(i = 0; i < customFactories.Length; ++i) {
				var factory = customFactories[i];
				if(factory.CanCreateFor(type)) {
					result = factory.CreateFor(type);
					contractMap.Add(type, result);
					return true;
				}
			}
			// No dice.
			result = null;
			return false;
		}

		private IContract GenerateContract(Type type) {
			if(type.IsPrimitive)
				throw new ArgumentException("Cannot generate contract for primitive type.", "type");
			if(type.IsInterface)
				throw new ArgumentException("Cannot generate contract for interface.", "type");
			if(type.IsAbstract)
				throw new ArgumentException("Cannot generate contract for abstract class.", "type");
			if(type.IsGenericTypeDefinition)
				throw new ArgumentException("Cannot generate contract for generic type definition.", "type");

			var flags = BindingFlags.Instance;
			if(0 != (ContractMode & MemberInclusion.PublicFields))
				flags |= BindingFlags.Public;
			if(0 != (ContractMode & MemberInclusion.NonPublicFields))
				flags |= BindingFlags.NonPublic;
			var fields = type.GetAllFields(flags);

			flags = BindingFlags.Instance;
			if(0 != (ContractMode & MemberInclusion.PublicProperties))
				flags |= BindingFlags.Public;
			if(0 != (ContractMode & MemberInclusion.NonPublicProperties))
				flags |= BindingFlags.NonPublic;
			var properties = type.GetAllProperties(flags);

			var result = new ILContract(type, fields, properties);
			contractMap.Add(type, result);
			return result;
		}

		public override string ToString() {
			string result = "Contractor";
			if(customContracts.Length != 0) {
				result += " Custom Contracts [" + string.Join(" | ", customContracts.Select(c => c.ToString()).ToArray()) + "]";
			}

			if(customFactories.Length != 0) {
				result += " Custom Factories [" + string.Join(" | ", customFactories.Select(c => c.ToString()).ToArray()) + "]";
			}

			return result;
		}

		#endregion

		#region Constructors

		static Contractor() {
			Default = new Contractor();
		}
		
		public Contractor() {
			ContractMode = MemberInclusion.AllFields;
			customContracts = noCustomContracts;
			customFactories = noCustomFactories;
			arrayContractFactory = new ArrayContractFactory();
			collectionContractFactory = new CollectionContractFactory();
			keyValuePairContractFactory = new KeyValuePairContractFactory();
		}

		public Contractor(MemberInclusion mode) {
			ContractMode = mode;
			customContracts = noCustomContracts;
			customFactories = noCustomFactories;
			arrayContractFactory = new ArrayContractFactory();
			collectionContractFactory = new CollectionContractFactory();
			keyValuePairContractFactory = new KeyValuePairContractFactory();
		}
		
		public Contractor(MemberInclusion mode, ICollection<IContract> contracts, ICollection<IContractFactory> factories) {
			ContractMode = mode;
			customContracts = null == contracts ? noCustomContracts : contracts.ToArray();
			customFactories = null == factories ? noCustomFactories : factories.ToArray();
			arrayContractFactory = new ArrayContractFactory();
			collectionContractFactory = new CollectionContractFactory();
			keyValuePairContractFactory = new KeyValuePairContractFactory();
		}
		
		public Contractor(MemberInclusion mode, params IContract[] contracts) : this(mode, contracts, null) { }
		
		public Contractor(MemberInclusion mode, params IContractFactory[] factories) : this(mode, null, factories) { }

		public Contractor(ICollection<IContract> contracts, ICollection<IContractFactory> factories) {
			ContractMode = MemberInclusion.AllFields;
			customContracts = null == contracts ? noCustomContracts : contracts.ToArray();
			customFactories = null == factories ? noCustomFactories : factories.ToArray();
			arrayContractFactory = new ArrayContractFactory();
			collectionContractFactory = new CollectionContractFactory();
			keyValuePairContractFactory = new KeyValuePairContractFactory();
		}

		public Contractor(params IContract[] contracts) : this(MemberInclusion.AllFields, contracts, null) { }

		public Contractor(params IContractFactory[] factories) : this(MemberInclusion.AllFields, null, factories) { }

		#endregion

		#region Fields

		private readonly IContract[] customContracts;
		private readonly IContractFactory[] customFactories;

		private readonly ArrayContractFactory arrayContractFactory;
		private readonly CollectionContractFactory collectionContractFactory;
		private readonly KeyValuePairContractFactory keyValuePairContractFactory;
		
		private readonly Dictionary<Type, IContract> contractMap = new Dictionary<Type, IContract>();

		private readonly static IContract[] noCustomContracts = new IContract[0];
		private readonly static IContractFactory[] noCustomFactories = new IContractFactory[0];

		#endregion
	}
}
