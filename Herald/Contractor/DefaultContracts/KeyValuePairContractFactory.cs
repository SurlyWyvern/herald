﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace SurlyWyvern.Herald.DefaultContracts {
	/// <summary>
	/// Generates contracts for the KeyValuePair struct.
	/// </summary>
	/// <remarks>
	/// This allows dictionaries to be serialized via their ICollection implementation.
	/// </remarks>
	public class KeyValuePairContractFactory : IContractFactory {
		#region Methods

		public bool CanCreateFor(Type type) {
			return null != type && type.IsGenericType && type.GetGenericTypeDefinition() == KVPType;
		}

		public IContract CreateFor(Type type) {
			var contractType = ContractType.MakeGenericType(type.GetGenericArguments());
			return (IContract)Activator.CreateInstance(contractType);
		}

		#endregion

		#region Fields

		public static readonly Type KVPType = typeof(KeyValuePair<,>);
		public static readonly Type ContractType = typeof(KeyValuePairContract<,>);

		#endregion
		
		private class KeyValuePairContract<K, V> : IContract {
			#region Methods

			public bool CanProcess(Type type) {
				if(null == type || !type.IsGenericType || type.GetGenericTypeDefinition() != typeof(KeyValuePair<,>))
					return false;
				var args = type.GetGenericArguments();
				return args[0] == KeyType && args[1] == ValueType;
			}

			public object Read(Deserializer deserializer, Type type = null, object existingValue = null) {
				return new KeyValuePair<K, V>((K)deserializer.Deserialize(),
					(V)deserializer.Deserialize());
			}

			public void Write(Serializer serializer, object value) {
				var kvp = (KeyValuePair<K, V>)value;
				serializer.SerializeExplicit(kvp.Key);
				serializer.SerializeExplicit(kvp.Value);
			}

			public override string ToString() {
				return "KeyValuePairContract<" + KeyType + ", " + ValueType + ">";
			}

			#endregion

			#region Constructor

			public KeyValuePairContract() {
				//Generate Write
				var writer = new DynamicMethod("Write", typeof(void), new Type[] { typeof(Serializer), KeyType, ValueType });
				var ilGen = writer.GetILGenerator();
				// Write key
				ilGen.Emit(OpCodes.Ldarg_0);
				ilGen.Emit(OpCodes.Ldarg_1);
				Reflection.EmitSerialize(KeyType, ilGen);

				// Write Value
				ilGen.Emit(OpCodes.Ldarg_0);
				ilGen.Emit(OpCodes.Ldarg_2);
				Reflection.EmitSerialize(ValueType, ilGen);
				
				ilGen.Emit(OpCodes.Ret);
				this.writer = (Action<Serializer, K, V>)writer.CreateDelegate(typeof(Action<Serializer, K, V>));


				// Generate Read
				var reader = new DynamicMethod("Read", typeof(KeyValuePair<K, V>), new Type[] { typeof(Deserializer) });
				ilGen = reader.GetILGenerator();
				// Read key
				ilGen.Emit(OpCodes.Ldarg_0);
				Reflection.EmitDeserialize(KeyType, ilGen);

				// Read value
				ilGen.Emit(OpCodes.Ldarg_0);
				Reflection.EmitDeserialize(ValueType, ilGen);

				// Create
				var ctor = typeof(KeyValuePair<K, V>).GetConstructor(new Type[] { KeyType, ValueType });
				ilGen.Emit(OpCodes.Newobj, ctor);
				ilGen.Emit(OpCodes.Ret);
				this.reader = (Func<Deserializer, KeyValuePair<K, V>>)reader.CreateDelegate(typeof(Func<Deserializer, KeyValuePair<K, V>>));

			}

			#endregion

			#region Fields

			private readonly Action<Serializer, K, V> writer;
			private readonly Func<Deserializer, KeyValuePair<K, V>> reader;

			public static readonly Type KeyType = typeof(K);
			public static readonly Type ValueType = typeof(V);

			#endregion
		}
		
	}
}
