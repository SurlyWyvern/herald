﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Linq;

namespace SurlyWyvern.Herald.DefaultContracts {
	/// <summary>
	/// Generates contracts for implementations of ICollection<T>.
	/// </summary>
	public class CollectionContractFactory : IContractFactory {
		#region Methods

		public bool CanCreateFor(Type type) {
			return type != null && type.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == InterfaceType);
		}

		public IContract CreateFor(Type type) {
			var elementType = type.GetInterfaces().Single(i => i.IsGenericType && i.GetGenericTypeDefinition() == InterfaceType).GetGenericArguments()[0];
			return (IContract)Activator.CreateInstance(ContractType.MakeGenericType(elementType));
		}

		#endregion

		#region Fields

		public static readonly Type InterfaceType = typeof(ICollection<>);
		public static readonly Type ContractType = typeof(CollectionContract<>);

		#endregion
		
		/// <summary>
		/// A Contract for implementations for ICollection<T>.
		/// </summary>
		private class CollectionContract<T> : IContract {
			#region Methods

			public bool CanProcess(Type type) {
				return CollectionType.IsAssignableFrom(type);
			}

			public object Read(Deserializer deserializer, Type type = null, object existingValue = null) {
				var collection = (ICollection<T>)existingValue;
				int count = deserializer.ReadInt32();
				if(count < 0)
					throw new ArgumentOutOfRangeException("count", "Negative collection size.");
				for(; count > 0; --count) {
					collection.Add(readElement(deserializer));
				}
				return collection;
			}

			public void Write(Serializer serializer, object value) {
				var collection = (ICollection<T>)value;
				serializer.Serialize(collection.Count);
				foreach(var element in collection) {
					writeElement(serializer, element);
				}
			}

			public override string ToString() {
				return "CollectionContract<" + ElementType.Name + ">";
			}

			#endregion

			#region Constructors

			public CollectionContract() {
				var writer = new DynamicMethod("WriteElement", typeof(void), writeParams);
				var ilGen = writer.GetILGenerator();
				ilGen.Emit(OpCodes.Ldarg_0);
				ilGen.Emit(OpCodes.Ldarg_1);
				Reflection.EmitSerialize(ElementType, ilGen);
				ilGen.Emit(OpCodes.Ret);
				writeElement = (Action<Serializer, T>)writer.CreateDelegate(typeof(Action<Serializer, T>));

				var reader = new DynamicMethod("ReadElement", ElementType, readParams);
				ilGen = reader.GetILGenerator();
				ilGen.Emit(OpCodes.Ldarg_0);
				Reflection.EmitDeserialize(ElementType, ilGen);
				ilGen.Emit(OpCodes.Ret);
				readElement = (Func<Deserializer, T>)reader.CreateDelegate(typeof(Func<Deserializer, T>));
			}

			#endregion

			#region Fields

			private readonly Func<Deserializer, T> readElement;
			private readonly Action<Serializer, T> writeElement;

			public static readonly Type CollectionType = typeof(ICollection<T>);
			public static readonly Type ElementType = typeof(T);
			private static readonly Type[] writeParams = new Type[] { typeof(Serializer), ElementType };
			private static readonly Type[] readParams = new Type[] { typeof(Deserializer) };

			#endregion
		}
		
	}
}
