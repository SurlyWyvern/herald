﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;

namespace SurlyWyvern.Herald.DefaultContracts {
	/// <summary>
	/// Contract that generates its Write method at runtime through Reflection.Emit.
	/// </summary>
	/// <remarks>
	/// Format: Member count, followed by member name/value pairs.
	/// </remarks>
	public class ILContract : IContract {
		/// <summary>
		/// Used for deserializing members of contracted types.
		/// </summary>
		public class Member {
			/// <summary>
			/// Signature for methods that read the value of the member from the
			/// deserializer and assign it to an instance of the contracted type.
			/// </summary>
			public delegate void SetValue(Deserializer deserializer, ref object instance);
			
			#region Properties
			
			public string Name {
				get;
			}
			
			public Type Type {
				get;
			}

			public SetValue Set {
				get;
			}

			#endregion

			#region Methods

			public override string ToString() {
				return "ILContract.Member " + Name + "<" + Type + ">";
			}

			#endregion

			#region Constructors

			private Member() {}

			public Member(FieldInfo field) {
				if(null == field)
					throw new ArgumentNullException("field");

				Name = field.Name;
				Type = field.FieldType;
				
				// Generate set method.
				var setter = new DynamicMethod("SetValue", typeof(void), setValueParams, field.DeclaringType, true);
				var ilGen = setter.GetILGenerator();

				if(field.DeclaringType.IsValueType) {
					// Unbox the instance and store it in a local var
					var unboxedInstance = ilGen.DeclareLocal(field.DeclaringType);
					ilGen.Emit(OpCodes.Ldarg_1);
					ilGen.Emit(OpCodes.Ldind_Ref);
					ilGen.Emit(OpCodes.Unbox_Any, field.DeclaringType);
					ilGen.Emit(OpCodes.Stloc_0);
					ilGen.Emit(OpCodes.Ldloca_S, 0);

					// Read the new value from the deserializer and assign it to the
					// relevant field.
					ilGen.Emit(OpCodes.Ldarg_0);
					Reflection.EmitDeserialize(field.FieldType, ilGen);
					ilGen.Emit(OpCodes.Stfld, field);
					
					// Assign the altered instance to the ref parameter.
					ilGen.Emit(OpCodes.Ldarg_1);
					ilGen.Emit(OpCodes.Ldloc_0);
					ilGen.Emit(OpCodes.Box, field.DeclaringType);
					ilGen.Emit(OpCodes.Stind_Ref);
					ilGen.Emit(OpCodes.Ret);
				} else {
					// Cast the reference to the correct type.
					ilGen.Emit(OpCodes.Ldarg_1);
					ilGen.Emit(OpCodes.Ldind_Ref);
					ilGen.Emit(OpCodes.Unbox_Any, field.DeclaringType);

					// Read the new value from the deserializer.
					ilGen.Emit(OpCodes.Ldarg_0);
					Reflection.EmitDeserialize(field.FieldType, ilGen);
					ilGen.Emit(OpCodes.Stfld, field);
					ilGen.Emit(OpCodes.Ret);
				}
				Set = (SetValue)setter.CreateDelegate(typeof(SetValue));
			}

			public Member(PropertyInfo property) {
				if(null == property)
					throw new ArgumentNullException("property");
				if(!property.CanRead)
					throw new ArgumentException("Property " + property.DeclaringType + "." + property.Name + " is writeonly.");
				if(!property.CanWrite)
					throw new ArgumentException("Property " + property.DeclaringType + "." + property.Name + " is readonly.");
				if(property.GetIndexParameters().Length != 0)
					throw new ArgumentException("Property " + property.DeclaringType + "." + property.Name + " is an indexer.");

				Name = property.Name;
				Type = property.PropertyType;
				
				var setter = new DynamicMethod("SetValue", typeof(void), setValueParams, property.DeclaringType, true);
				var ilGen = setter.GetILGenerator();
				
				if(property.DeclaringType.IsValueType) {
					// Unbox the instance
					var unboxedInstance = ilGen.DeclareLocal(property.DeclaringType);
					ilGen.Emit(OpCodes.Ldarg_1);
					ilGen.Emit(OpCodes.Ldind_Ref);
					ilGen.Emit(OpCodes.Unbox_Any, property.DeclaringType);
					ilGen.Emit(OpCodes.Stloc_0);
					ilGen.Emit(OpCodes.Ldloca, 0);

					// Read the value.
					ilGen.Emit(OpCodes.Ldarg_0);
					Reflection.EmitDeserialize(property.PropertyType, ilGen);
					// Call the property setter
					ilGen.Emit(OpCodes.Call, property.GetSetMethod(true));
					
					ilGen.Emit(OpCodes.Ldarg_1);
					ilGen.Emit(OpCodes.Ldloc_0);
					ilGen.Emit(OpCodes.Box, property.DeclaringType);
					ilGen.Emit(OpCodes.Stind_Ref);
					ilGen.Emit(OpCodes.Ret);
				} else {
					// Load instance.
					ilGen.Emit(OpCodes.Ldarg_1);
					ilGen.Emit(OpCodes.Ldind_Ref);
					ilGen.Emit(OpCodes.Castclass, property.DeclaringType);

					// Read value.
					ilGen.Emit(OpCodes.Ldarg_0);
					Reflection.EmitDeserialize(property.PropertyType, ilGen);
					ilGen.Emit(OpCodes.Call, property.GetSetMethod(true));
					ilGen.Emit(OpCodes.Ret);
				}
				Set = (SetValue)setter.CreateDelegate(typeof(SetValue));
			}

			#endregion

			#region Fields
			
			private static readonly Type[] setValueParams = new Type[] { typeof(Deserializer), typeof(object).MakeByRefType() };
			
			#endregion
		}

		#region Properties

		/// <summary>
		/// Type that this contract can process.
		/// </summary>
		public Type Type {
			get;
			private set;
		}

		#endregion

		#region Methods

		public bool CanProcess(Type type) {
			return Type == type;
		}
		
		public object Read(Deserializer deserializer, Type type = null, object existingValue = null) {
			if(null == existingValue)
				existingValue = Deserializer.CreateInstance(Type);
			int count = deserializer.ReadInt32();
			if(count < 0)
				throw new SerializationException(ToString() + " read a negative member count.");
			for(; count > 0; --count) {
				var name = (string)deserializer.Deserialize(typeof(string));
				if(null == name)
					throw new NullReferenceException(ToString() + " read a null member name.");
				var member = Array.Find(members, x => x.Name == name);
				if(null == member)
					throw new SerializationException(ToString() + " does not serialize a member with the name '" + name + "'");
				member.Set(deserializer, ref existingValue);
			}
			return existingValue;
		}
		
		public void Write(Serializer serializer, object value) {
			serializer.Serialize(members.Length);
			write(serializer, value);
		}

		public Member[] GetMembers() {
			var result = new Member[members.Length];
			members.CopyTo(result, 0);
			return result;
		}

		public override string ToString() => "ILContract<" + Type.Name + ">";
		
		#endregion

		#region Constructors
		
		private ILContract() { }
		
		public ILContract(Type type, FieldInfo[] fields, PropertyInfo[] properties) {
			Type = type ?? throw new ArgumentNullException("type");
			if(null == fields)
				fields = noFields;
			if(null == properties)
				properties = noProperties;
			
			#region Write Method
			
			var writeMethod = new DynamicMethod("Write",
				typeof(void),
				new Type[] { typeof(Serializer), typeof(object) },
				type,
				true);
			var writeGen = writeMethod.GetILGenerator();

			if(type.IsValueType) {
				// Unbox the struct and store it in a local.
				var valueTypeTemp = writeGen.DeclareLocal(type);
				writeGen.Emit(OpCodes.Ldarg_1);
				writeGen.Emit(OpCodes.Unbox_Any, type);
				writeGen.Emit(OpCodes.Stloc_0);

				foreach(var field in fields) {
					// Serialize Field name.
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldstr, field.Name);
					writeGen.Emit(OpCodes.Call, Reflection.Serialize);
					// Serialize value.
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldloc_0);
					writeGen.Emit(OpCodes.Ldfld, field);
					Reflection.EmitSerialize(field.FieldType, writeGen);
				}

				foreach(var property in properties) {
					// Write Property Name
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldstr, property.Name);
					writeGen.Emit(OpCodes.Call, Reflection.Serialize);

					// Get value via property getter and serialize.
					var getter = property.GetGetMethod(true);
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldloca, 0);
					writeGen.Emit(OpCodes.Call, getter);
					Reflection.EmitSerialize(property.PropertyType, writeGen);
				}

			} else {

				foreach(var field in fields) {
					// Serialize field name
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldstr, field.Name);
					writeGen.Emit(OpCodes.Call, Reflection.Serialize);
					// Serialize value.
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldarg_1);
					writeGen.Emit(OpCodes.Ldfld, field);
					Reflection.EmitSerialize(field.FieldType, writeGen);
				}

				foreach(var property in properties) {
					// Write Property Name
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldstr, property.Name);
					writeGen.Emit(OpCodes.Call, Reflection.Serialize);

					// Get value via property getter and serialize.
					var getter = property.GetGetMethod(true);
					writeGen.Emit(OpCodes.Ldarg_0);
					writeGen.Emit(OpCodes.Ldarg_1);
					writeGen.Emit(OpCodes.Call, getter);
					Reflection.EmitSerialize(property.PropertyType, writeGen);
				}
			}

			writeGen.Emit(OpCodes.Ret);

			write = (Action<Serializer, object>)writeMethod.CreateDelegate(typeof(Action<Serializer, object>));

			#endregion

			members = new Member[fields.Length + properties.Length];
			int i = 0;
			for(; i < fields.Length; ++i) {
				members[i] = new Member(fields[i]);
			}
			for(; i < members.Length; ++i) {
				members[i] = new Member(properties[i - fields.Length]);
			}
		}

		#endregion

		#region Fields
		
		private readonly Member[] members;
		private readonly Action<Serializer, object> write;

		private readonly static FieldInfo[] noFields = new FieldInfo[0];
		private readonly static PropertyInfo[] noProperties = new PropertyInfo[0];
		
		#endregion
	}
}
