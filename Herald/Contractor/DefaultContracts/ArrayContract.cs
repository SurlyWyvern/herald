﻿using System;
using System.Reflection.Emit;

namespace SurlyWyvern.Herald.DefaultContracts {
	/// <summary>
	/// Generates contracts for single-dimensional array.
	/// </summary>
	public class ArrayContractFactory : IContractFactory {
		public bool CanCreateFor(Type type) {
			return null != type && type.IsArray && type.GetArrayRank() == 1;
		}
		
		public IContract CreateFor(Type type) {
			return (IContract)Activator.CreateInstance(ContractType.MakeGenericType(type.GetElementType()));
		}

		#region Fields

		public static readonly Type ContractType = typeof(ArrayContract<>);

		#endregion

		private class ArrayContract<T> : IContract {
			#region Methods

			public bool CanProcess(Type type) {
				return null != type && type == ArrayType;
			}

			public object Read(Deserializer deserializer, Type type = null, object existingValue = null) {
				var array = (T[])existingValue;
				var length = array.Length;
				for(int i = 0; i < length; ++i)
					array[i] = readElement(deserializer);
				return array;
			}

			public void Write(Serializer serializer, object value) {
				var array = (T[])value;
				int length = array.Length;
				serializer.Serialize(length);
				for(int i = 0; i < length; ++i)
					writeElement(serializer, array[i]);
			}

			public override string ToString() {
				return "ArrayContract<" + ElementType + ">";
			}

			#endregion

			#region Constructors

			public ArrayContract() {
				var writer = new DynamicMethod("WriteElement", typeof(void), writeParams);
				var ilGen = writer.GetILGenerator();
				ilGen.Emit(OpCodes.Ldarg_0);
				ilGen.Emit(OpCodes.Ldarg_1);
				Reflection.EmitSerialize(ElementType, ilGen);
				ilGen.Emit(OpCodes.Ret);
				writeElement = (Action<Serializer, T>)writer.CreateDelegate(typeof(Action<Serializer, T>));

				var reader = new DynamicMethod("ReadElement", ElementType, readParams);
				ilGen = reader.GetILGenerator();
				ilGen.Emit(OpCodes.Ldarg_0);
				Reflection.EmitDeserialize(ElementType, ilGen);
				ilGen.Emit(OpCodes.Ret);
				readElement = (Func<Deserializer, T>)reader.CreateDelegate(typeof(Func<Deserializer, T>));
			}

			#endregion

			#region Fields

			private readonly Func<Deserializer, T> readElement;
			private readonly Action<Serializer, T> writeElement;

			public static readonly Type ElementType = typeof(T);
			public static readonly Type ArrayType = typeof(T[]);
			private static readonly Type[] writeParams = new Type[] { typeof(Serializer), ElementType };
			private static readonly Type[] readParams = new Type[] { typeof(Deserializer) };


			#endregion
		}

	}
}
