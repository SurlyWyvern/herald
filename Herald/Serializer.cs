﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Runtime.InteropServices;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Serializes data to a stream in a binary format.
	/// </summary>
	public class Serializer : IDisposable {
		#region Properties

		/// <summary>
		/// If true, the stream will not be closed when this instance is disposed of.
		/// </summary>
		public bool KeepOpen {
			get;
			set;
		}

		/// <summary>
		/// Returns true if underlying stream can be written to.
		/// </summary>
		public bool Open {
			get { return null != stream; }
		}

		/// <summary>
		/// Whether or not subequent appearances of reference types should be
		/// replaced with a reference ID.
		/// </summary>
		public bool ReferenceHandling {
			get => session.ReferenceHandling;
		}

		/// <summary>
		/// <see cref="Contractor"/> instance for this instance's session.
		/// </summary>
		public IContractor Contractor {
			get => session.Contractor;
		}

		/// <summary>
		/// <see cref="AliasBinder"/> instance for this instance's session.
		/// </summary>
		public AliasBinder Binder {
			get => session.AliasBinder;
		}

		public Encoding Encoding {
			get;
		} = Encoding.UTF8;

		#endregion

		#region Methods

		/// <summary>
		/// Serialize the given object to the output stream.
		/// </summary>
		/// <remarks>
		/// The value will be prefixed with full type information, allowing it to 
		/// be deserialized without specifying the type.
		/// </remarks>
		public void SerializeExplicit(object value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(null == value) {
				stream.WriteByte((byte)Token.Null);
				return;
			}

			if(ReferenceHandling && session.IsReferenced(value)) {
				Prefix(Token.Ref);
				Serialize(session.GetReferenceID(value));
				return;
			}

			var type = value.GetType();
			if(type.IsPrimitive) {
				WritePrimitive(value);
			} else if(type == typeof(Decimal)) {
				Prefix(Token.Decimal);
				Serialize((Decimal)value);
			} else if(type == typeof(DateTime)) {
				Prefix(Token.DateTime);
				Serialize((DateTime)value);
			} else if(type == typeof(TimeSpan)) {
				Prefix(Token.TimeSpan);
				Serialize((TimeSpan)value);
			} else if(type == typeof(DateTimeOffset)) {
				Prefix(Token.DateTimeOffset);
				Serialize((DateTimeOffset)value);
			} else if(type == typeof(Guid)) {
				Prefix(Token.Guid);
				Serialize((Guid)value);
			} else if(type == typeof(string)) {
				if(ReferenceHandling && !session.IsReferenced(value)) {
					Prefix(Token.StringRef);
					Serialize(session.MakeReference(value));
				} else {
					Prefix(Token.String);
				}
				Write((string)value);
			} else if(type.IsEnum) {
				Prefix(Token.Enum);
				SerializeExplicit(type);
				Serialize(Convert.ChangeType(value, Enum.GetUnderlyingType(type)));
			} else if(typeof(Type).IsAssignableFrom(type)) {
				if(ReferenceHandling && !session.IsReferenced(value)) {
					Prefix(Token.TypeRef);
					Serialize(session.MakeReference(value));
				} else {
					Prefix(Token.Type);
				}
				Write(Binder.BindToAlias((Type)value));
			} else if(type.IsValueType) {
				Prefix(Token.Struct);
				SerializeExplicit(type);
				if(value is ISelfContract selfContract)
					selfContract.Write(this);
				else
					Contractor.GetContract(type).Write(this, value);
			} else {

				if(!ReferenceHandling) {
					if(serializeStack.Contains(value)) {
						if(session.ReferenceLoopHandling == ReferenceLoopHandling.Nullify) {
							stream.WriteByte((byte)Token.Null);
							return;
						} else {
							throw new SerializationException("Self-referencing loop detected; value '" + value + "' already exists on the serialization stack.");
						}
					} else {
						serializeStack.Push(value);
					}
				}

				if(type.IsArray && type.GetArrayRank() == 1) {
					if(ReferenceHandling && !session.IsReferenced(value)) {
						Prefix(Token.ArrayRef);
						Serialize(session.MakeReference(value));
					} else {
						Prefix(Token.Array);
					}
					var elementType = type.GetElementType();
					SerializeExplicit(elementType);
					if(elementType.IsPrimitive)
						Write((Array)value);
					else
						Contractor.GetContract(type).Write(this, value);
				} else {
					if(ReferenceHandling && !session.IsReferenced(value)) {
						Prefix(Token.ObjectRef);
						Serialize(session.MakeReference(value));
					} else {
						Prefix(Token.Object);
					}
					SerializeExplicit(type);
					if(value is ISelfContract selfContract)
						selfContract.Write(this);
					else
						Contractor.GetContract(type).Write(this, value);
				}

				if(!ReferenceHandling && !ReferenceEquals(serializeStack.Pop(), value))
					throw new SerializationException("Popped value from serialization stack did not match serialized value.");
			}
		}

		/// <summary>
		/// Overload for implementations of ISelfContract.
		/// </summary>
		public void SerializeExplicit(ISelfContract value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			if(null == value) {
				stream.WriteByte((byte)Token.Null);
				return;
			}

			if(ReferenceHandling && session.IsReferenced(value)) {
				Prefix(Token.Ref);
				Serialize(session.GetReferenceID(value));
				return;
			}

			var type = value.GetType();
			if(type.IsValueType) {
				Prefix(Token.Struct);
				Serialize(type);
				value.Write(this);
			} else {

				if(!ReferenceHandling) {
					if(serializeStack.Contains(value)) {
						if(session.ReferenceLoopHandling == ReferenceLoopHandling.Nullify) {
							stream.WriteByte((byte)Token.Null);
							return;
						} else {
							throw new SerializationException("Self-referencing loop detected; value '" + value + "' already exists on the serialization stack.");
						}
					} else {
						serializeStack.Push(value);
					}
				}

				if(ReferenceHandling && !session.IsReferenced(value)) {
					Prefix(Token.ObjectRef);
					Serialize(session.MakeReference(value));
				} else {
					Prefix(Token.Object);
				}
				Serialize(type);
				value.Write(this);
				if(!ReferenceHandling && !ReferenceEquals(serializeStack.Pop(), value))
					throw new SerializationException("Popped value from serialization stack did not match serialized value.");
			}
		}

		/// <summary>
		/// Overload for Type.
		/// </summary>
		public void SerializeExplicit(Type value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(null == value) {
				stream.WriteByte((byte)Token.Null);
				return;
			}

			if(ReferenceHandling) {
				if(session.IsReferenced(value)) {
					Prefix(Token.Ref);
					Serialize(session.GetReferenceID(value));
				} else {
					Prefix(Token.TypeRef);
					Serialize(session.MakeReference(value));
					Write(Binder.BindToAlias(value));
				}
			} else {
				Prefix(Token.Type);
				Write(Binder.BindToAlias(value));
			}
		}

		/// <summary>
		/// Overload for string.
		/// </summary>
		public void SerializeExplicit(string value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(null == value) {
				stream.WriteByte((byte)Token.Null);
				return;
			}

			if(ReferenceHandling) {
				if(session.IsReferenced(value)) {
					Prefix(Token.Ref);
					Serialize(session.GetReferenceID(value));
				} else {
					Prefix(Token.StringRef);
					Serialize(session.MakeReference(value));
					Write(value);
				}
			} else {
				Prefix(Token.String);
				Write(value);
			}
		}

		/// <summary>
		/// Serialize the given object to the output stream.
		/// </summary>
		public void Serialize(object value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(null == value) {
				stream.WriteByte((byte)Token.Null);
				return;
			}

			if(ReferenceHandling && session.IsReferenced(value)) {
				Prefix(Token.Ref);
				Serialize(session.GetReferenceID(value));
				return;
			}

			var type = value.GetType();
			if(type.IsPrimitive) {
				switch(Type.GetTypeCode(type)) {
					case TypeCode.Boolean:
						Serialize((bool)value);
						break;
					case TypeCode.Byte:
						Serialize((byte)value);
						break;
					case TypeCode.Int16:
						Serialize((Int16)value);
						break;
					case TypeCode.Int32:
						Serialize((Int32)value);
						break;
					case TypeCode.Int64:
						Serialize((Int64)value);
						break;
					case TypeCode.Single:
						Serialize((Single)value);
						break;
					case TypeCode.Double:
						Serialize((Double)value);
						break;
					case TypeCode.SByte:
						Serialize((SByte)value);
						break;
					case TypeCode.Char:
						Serialize((Char)value);
						break;
					case TypeCode.UInt16:
						Serialize((UInt16)value);
						break;
					case TypeCode.UInt32:
						Serialize((UInt32)value);
						break;
					case TypeCode.UInt64:
						Serialize((UInt64)value);
						break;
					default:
						throw new ArgumentException("Unknown primitive type: " + type);
				}
			} else if(type == typeof(Decimal)) {
				Serialize((Decimal)value);
			} else if(type == typeof(DateTime)) {
				Serialize((DateTime)value);
			} else if(type == typeof(TimeSpan)) {
				Serialize((TimeSpan)value);
			} else if(type == typeof(DateTimeOffset)) {
				Serialize((DateTimeOffset)value);
			} else if(type == typeof(Guid)) {
				Serialize((Guid)value);
			} else if(type == typeof(string)) {
				if(ReferenceHandling && !session.IsReferenced(value)) {
					Prefix(Token.StringRef);
					Serialize(session.MakeReference(value));
				} else {
					Prefix(Token.String);
				}
				Write((string)value);
			} else if(type.IsEnum) {
				Serialize(Convert.ChangeType(value, Enum.GetUnderlyingType(type)));
			} else if(typeof(Type).IsAssignableFrom(type)) {
				if(ReferenceHandling && !session.IsReferenced(value)) {
					Prefix(Token.TypeRef);
					Serialize(session.MakeReference(value));
				} else {
					Prefix(Token.Type);
				}
				Write(Binder.BindToAlias((Type)value));
			} else if(type.IsValueType) {
				if(value is ISelfContract selfContract)
					selfContract.Write(this);
				else
					Contractor.GetContract(type).Write(this, value);
			} else {
				if(!ReferenceHandling) {
					if(serializeStack.Contains(value)) {
						if(session.ReferenceLoopHandling == ReferenceLoopHandling.Nullify) {
							stream.WriteByte((byte)Token.Null);
							return;
						} else {
							throw new SerializationException("Self-referencing loop detected; value '" + value + "' already exists on the serialization stack.");
						}
					} else {
						serializeStack.Push(value);
					}
				}
				
				if(type.IsArray && type.GetArrayRank() == 1) {
					if(ReferenceHandling && !session.IsReferenced(value)) {
						Prefix(Token.ArrayRef);
						Serialize(session.MakeReference(value));
					} else {
						Prefix(Token.Array);
					}

					if(type.GetElementType().IsPrimitive)
						Write((Array)value);
					else
						Contractor.GetContract(type).Write(this, value);
				} else {
					if(ReferenceHandling && !session.IsReferenced(value)) {
						Prefix(Token.ObjectRef);
						Serialize(session.MakeReference(value));
					} else {
						Prefix(Token.Object);
					}
					if(value is ISelfContract selfContract)
						selfContract.Write(this);
					else
						Contractor.GetContract(type).Write(this, value);
				}

				if(!ReferenceHandling && !ReferenceEquals(serializeStack.Pop(), value))
					throw new SerializationException("Popped value from serialization stack did not match serialized value.");
			}
		}

		/// <summary>
		/// Overload for nullable values.
		/// </summary>
		/// <remarks>
		/// Prefixing a nullable value is required even with implicit serialization
		/// because a null value requires a token to be serialized.
		/// </remarks>
		public void Serialize<T>(T? value) where T : struct {
			if(!value.HasValue) {
				stream.WriteByte((byte)Token.Null);
			} else {
				SerializeNullable(value, typeof(T));
			}
		}

		/// <summary>
		/// Overload for implementations of <see cref="ISelfContract"/>.
		/// </summary>
		public void Serialize(ISelfContract value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			if(null == value) {
				stream.WriteByte((byte)Token.Null);
				return;
			}

			if(ReferenceHandling && session.IsReferenced(value)) {
				Prefix(Token.Ref);
				Serialize(session.GetReferenceID(value));
				return;
			}

			var type = value.GetType();
			if(type.IsValueType) {
				value.Write(this);
			} else {

				if(!ReferenceHandling) {
					if(serializeStack.Contains(value)) {
						if(session.ReferenceLoopHandling == ReferenceLoopHandling.Nullify) {
							stream.WriteByte((byte)Token.Null);
							return;
						} else {
							throw new SerializationException("Self-referencing loop detected; value '" + value + "' already exists on the serialization stack.");
						}
					} else {
						serializeStack.Push(value);
					}
				}

				if(ReferenceHandling && !session.IsReferenced(value)) {
					Prefix(Token.ObjectRef);
					Serialize(session.MakeReference(value));
				} else {
					Prefix(Token.Object);
				}
				value.Write(this);
				if(!ReferenceHandling && !ReferenceEquals(serializeStack.Pop(), value))
					throw new SerializationException("Popped value from serialization stack did not match serialized value.");
			}
		}

		/// <summary>
		/// Overload for Type.
		/// </summary>
		public void Serialize(Type value) {
			// Types always require prefixing
			SerializeExplicit(value);
		}

		/// <summary>
		/// Overload for string.
		/// </summary>
		public void Serialize(string value) {
			SerializeExplicit(value);
		}
		
		/// <summary>
		/// Serializes the given value, prefixing it with a byte token that indicates
		/// whether it is an instance or subclass of the given type.
		/// </summary>
		public void Serialize(object value, Type containerType) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			if(null == containerType)
				throw new ArgumentNullException("containerType");
			if(containerType == typeof(object)) {
				SerializeExplicit(value);
				return;
			}

			if(null == value) {
				stream.WriteByte((byte)PolyToken.Null);
				return;
			}

			var type = value.GetType();
			if(type.IsValueType)
				throw new ArgumentException("value is a Value Type.");

			if(type == containerType) {
				Serialize((byte)PolyToken.Instance);
				Serialize(value);
			} else {
				Serialize((byte)PolyToken.Inheritor);
				Serialize(type);
				Serialize(value);
			}
		}

		/// <summary>
		/// Serialize a boxed nullable type.
		/// </summary>
		/// <param name="value">Boxed value of the nullable.</param>
		/// <param name="underlyingType">Underlying type of the nullable.</param>
		public void SerializeNullable(object value, Type underlyingType) {
			if(null == underlyingType)
				throw new ArgumentNullException("underlyingType");
			if(!underlyingType.IsValueType)
				throw new ArgumentException("Underlying type must be a value type.", "underlyingType");
			if(null != Nullable.GetUnderlyingType(underlyingType))
				throw new ArgumentException("Underlying type cannot be a nullable type.", "underlyingType");
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(null != value) {
				if(underlyingType.IsPrimitive) {
					WritePrimitive(value);
				} else if(underlyingType == typeof(Decimal)) {
					Prefix(Token.Decimal);
					Serialize((Decimal)value);
				} else if(underlyingType == typeof(DateTime)) {
					Prefix(Token.DateTime);
					Serialize((DateTime)value);
				} else if(underlyingType == typeof(TimeSpan)) {
					Prefix(Token.TimeSpan);
					Serialize((TimeSpan)value);
				} else if(underlyingType == typeof(DateTimeOffset)) {
					Prefix(Token.DateTimeOffset);
					Serialize((DateTimeOffset)value);
				} else if(underlyingType == typeof(Guid)) {
					Prefix(Token.Guid);
					Serialize((Guid)value);
				} else {
					Prefix(Token.Struct);
					if(value is ISelfContract selfContract)
						selfContract.Write(this);
					else
						Contractor.GetContract(underlyingType).Write(this, value);
				}
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		#region Primitives & Builtins

		/// <summary>
		/// Add a Token to the buffer without writing it to the output stream.
		/// </summary>
		private void Prefix(Token token) {
			buffer[pending++] = (byte)token;
		}

		/// <summary>
		/// Write a boolean value to the output stream.
		/// </summary>
		public void Serialize(bool value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			buffer[pending++] = (byte)(value ? 1 : 0);
			Flush();
		}

		/// <summary>
		/// Write a byte value to the output stream.
		/// </summary>
		public void Serialize(byte value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			stream.WriteByte(value);
		}

		/// <summary>
		/// Write a ushort value to the output stream.
		/// </summary>
		public void Serialize(ushort value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			buffer[pending++] = (byte)value;
			buffer[pending++] = (byte)(value >> 8);
			Flush();
		}

		/// <summary>
		/// Write a uint value to the output stream.
		/// </summary>
		public void Serialize(uint value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			buffer[pending++] = (byte)value;
			buffer[pending++] = (byte)(value >> 8);
			buffer[pending++] = (byte)(value >> 16);
			buffer[pending++] = (byte)(value >> 24);
			Flush();
		}

		/// <summary>
		/// Write a ulong value to the output stream.
		/// </summary>
		public void Serialize(ulong value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			buffer[pending++] = (byte)value;
			buffer[pending++] = (byte)(value >> 8);
			buffer[pending++] = (byte)(value >> 16);
			buffer[pending++] = (byte)(value >> 24);
			buffer[pending++] = (byte)(value >> 32);
			buffer[pending++] = (byte)(value >> 40);
			buffer[pending++] = (byte)(value >> 48);
			buffer[pending++] = (byte)(value >> 56);
			Flush();
		}

		/// <summary>
		/// Write a sbyte value to the output stream.
		/// </summary>
		public void Serialize(sbyte value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			stream.WriteByte((byte)value);
		}

		/// <summary>
		/// Write a short value to the output stream.
		/// </summary>
		public void Serialize(short value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			buffer[pending++] = (byte)value;
			buffer[pending++] = (byte)(value >> 8);
			Flush();
		}

		/// <summary>
		/// Write an int value to the output stream.
		/// </summary>
		public void Serialize(int value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			buffer[pending++] = (byte)value;
			buffer[pending++] = (byte)(value >> 8);
			buffer[pending++] = (byte)(value >> 16);
			buffer[pending++] = (byte)(value >> 24);
			Flush();
		}

		/// <summary>
		/// Write a long value to the output stream.
		/// </summary>
		public void Serialize(long value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			buffer[pending++] = (byte)value;
			buffer[pending++] = (byte)(value >> 8);
			buffer[pending++] = (byte)(value >> 16);
			buffer[pending++] = (byte)(value >> 24);
			buffer[pending++] = (byte)(value >> 32);
			buffer[pending++] = (byte)(value >> 40);
			buffer[pending++] = (byte)(value >> 48);
			buffer[pending++] = (byte)(value >> 56);
			Flush();
		}

		/// <summary>
		/// Write a float value to the output stream.
		/// </summary>
		public unsafe void Serialize(float value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			int temp = *(int*)(&value);
			Serialize(temp);
		}

		/// <summary>
		/// Write a double value to the output stream.
		/// </summary>
		public unsafe void Serialize(double value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			ulong temp = *(ulong*)(&value);
			Serialize(temp);
		}

		/// <summary>
		/// Write a char value to the output stream.
		/// </summary>
		public unsafe void Serialize(char value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			fixed (byte* pBuffer = buffer) {
				pending += Encoding.GetBytes(&value, 1, pBuffer + pending, buffer.Length - pending);
			}
			Flush();
		}

		/// <summary>
		/// Write a decimal value to the output stream.
		/// </summary>
		public void Serialize(decimal value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			DecimalCopier copy = DecimalCopier.Create(value);
			copy.CopyTo(buffer, pending);
			pending += 14;
			Flush();
		}

		/// <summary>
		/// Write a DateTime value to the output stream.
		/// </summary>
		public void Serialize(DateTime value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			var ticks = value.Ticks;
			buffer[pending++] = (byte)ticks;
			buffer[pending++] = (byte)(ticks >> 8);
			buffer[pending++] = (byte)(ticks >> 16);
			buffer[pending++] = (byte)(ticks >> 24);
			buffer[pending++] = (byte)(ticks >> 32);
			buffer[pending++] = (byte)(ticks >> 40);
			buffer[pending++] = (byte)(ticks >> 48);
			buffer[pending++] = (byte)(ticks >> 56);
			buffer[pending++] = (byte)value.Kind;
			Flush();
		}

		/// <summary>
		/// Write a TimeSpan value to the output stream.
		/// </summary>
		public void Serialize(TimeSpan value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			Serialize(value.Ticks);
		}

		/// <summary>
		/// Write a DateTimeOffset value to the output stream.
		/// </summary>
		public void Serialize(DateTimeOffset value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			var ticks = value.Ticks;
			buffer[pending++] = (byte)ticks;
			buffer[pending++] = (byte)(ticks >> 8);
			buffer[pending++] = (byte)(ticks >> 16);
			buffer[pending++] = (byte)(ticks >> 24);
			buffer[pending++] = (byte)(ticks >> 32);
			buffer[pending++] = (byte)(ticks >> 40);
			buffer[pending++] = (byte)(ticks >> 48);
			buffer[pending++] = (byte)(ticks >> 56);
			var offsetMins = (short)value.Offset.TotalMinutes;
			Serialize(offsetMins);
		}

		/// <summary>
		/// Write a Guid value to the output stream.
		/// </summary>
		public void Serialize(Guid value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			GuidCopier copy = GuidCopier.Create(value);
			copy.CopyTo(buffer, pending);
			pending += 16;
			Flush();
		}

		/// <summary>
		/// Write a nullable boolean to the output stream.
		/// </summary>
		public void Serialize(bool? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Boolean;
				buffer[pending++] = (byte)(value.Value ? 1 : 0);
				Flush();
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable byte to the output stream.
		/// </summary>
		public void Serialize(byte? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Byte;
				buffer[pending++] = value.Value;
				Flush();
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable ushort to the output stream.
		/// </summary>
		public void Serialize(ushort? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.UInt16;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable uint to the output stream.
		/// </summary>
		public void Serialize(uint? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.UInt32;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable ulong to the output stream.
		/// </summary>
		public void Serialize(ulong? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.UInt64;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable sbyte to the output stream.
		/// </summary>
		public void Serialize(sbyte? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.SByte;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable short to the output stream.
		/// </summary>
		public void Serialize(short? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Int16;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable int to the output stream.
		/// </summary>
		public void Serialize(int? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Int32;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable long to the output stream.
		/// </summary>
		public void Serialize(long? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Int64;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable float to the output stream.
		/// </summary>
		public void Serialize(float? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Float32;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable double to the output stream.
		/// </summary>
		public void Serialize(double? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Float64;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable char to the output stream.
		/// </summary>
		public void Serialize(char? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Char;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable decimal to the output stream.
		/// </summary>
		public void Serialize(decimal? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Decimal;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable DateTime to the output stream.
		/// </summary>
		public void Serialize(DateTime? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.DateTime;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable TimeSpan to the output stream.
		/// </summary>
		public void Serialize(TimeSpan? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.TimeSpan;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable DateTimeOffset to the output stream.
		/// </summary>
		public void Serialize(DateTimeOffset? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.DateTimeOffset;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		/// <summary>
		/// Write a nullable Guid to the output stream.
		/// </summary>
		public void Serialize(Guid? value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);

			if(value.HasValue) {
				buffer[pending++] = (byte)Token.Guid;
				Serialize(value.Value);
			} else {
				stream.WriteByte((byte)Token.Null);
			}
		}

		#endregion
		
		/// <summary>
		/// Write the contents of a byte array directly to the underlying stream.
		/// </summary>
		public void Write(byte[] value) {
			if(null == value)
				throw new ArgumentNullException("value");
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			stream.Write(value, 0, value.Length);
		}

		/// <summary>
		/// Write the contents of a byte array directly to the underlying stream.
		/// </summary>
		/// <param name="offset">Offset from array start to being writing.</param>
		/// <param name="count">Total number of bytes to write.</param>
		public void Write(byte[] value, int offset, int count) {
			if(null == value)
				throw new ArgumentNullException("value");
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			stream.Write(value, offset, count);
		}

		/// <summary>
		/// Write a string to the output stream.
		/// </summary>
		private unsafe void Write(string value) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			// Prefix string length
			int byteLength = Encoding.GetByteCount(value);
			{
				int copyLength = byteLength;
				while(copyLength >= 0x80) {
					buffer[pending++] = (byte)(copyLength | 0x80);
					copyLength >>= 7;
				}
				buffer[pending++] = (byte)(copyLength);
			}
			
			if(byteLength == 0) {
				Flush();
			} else if(pending + byteLength <= buffer.Length) {
				pending += Encoding.GetBytes(value, 0, value.Length, buffer, pending);
				Flush();
			} else {
				Flush();
				int maxCharsPerIter = buffer.Length / Encoding.GetMaxByteCount(1);
				int remainingChars = value.Length;
				while(remainingChars > 0) {
					fixed(char* pChar = value)
					fixed (byte* pBuffer = buffer) {
						int charsThisIter = remainingChars <= maxCharsPerIter ? remainingChars : maxCharsPerIter;
						pending = Encoding.GetBytes(pChar + (value.Length - remainingChars), charsThisIter, pBuffer, buffer.Length);
						Flush();
						remainingChars -= charsThisIter;
					}
				}
			}
		}

		/// <summary>
		/// Copies an array of primitives into the stream.
		/// </summary>
		private void Write(Array array) {
			// No. of elements.
			Serialize(array.Length);

			int progress = 0;
			int length = Buffer.ByteLength(array);
			while(progress < length) {
				int copyCount = length - progress;
				if(copyCount > buffer.Length)
					copyCount = buffer.Length;
				Buffer.BlockCopy(array, progress, buffer, 0, copyCount);
				stream.Write(buffer, 0, copyCount);
				progress += copyCount;
			}
		}

		private void Flush() {
			if(pending == 0)
				return;
			if(pending > buffer.Length)
				pending = buffer.Length;
			stream.Write(buffer, 0, pending);
			pending = 0;
		}

		private void WritePrimitive(object value) {
			if(null == value)
				throw new ArgumentNullException("value");

			var type = value.GetType();
			switch(Type.GetTypeCode(type)) {
				case TypeCode.Boolean:
					Prefix(Token.Boolean);
					Serialize((bool)value);
					break;
				case TypeCode.Byte:
					Prefix(Token.Byte);
					Serialize((byte)value);
					break;
				case TypeCode.Int16:
					Prefix(Token.Int16);
					Serialize((Int16)value);
					break;
				case TypeCode.Int32:
					Prefix(Token.Int32);
					Serialize((Int32)value);
					break;
				case TypeCode.Int64:
					Prefix(Token.Int64);
					Serialize((Int64)value);
					break;
				case TypeCode.Single:
					Prefix(Token.Float32);
					Serialize((Single)value);
					break;
				case TypeCode.Double:
					Prefix(Token.Float64);
					Serialize((Double)value);
					break;
				case TypeCode.SByte:
					Prefix(Token.SByte);
					Serialize((SByte)value);
					break;
				case TypeCode.Char:
					Prefix(Token.Char);
					Serialize((Char)value);
					break;
				case TypeCode.UInt16:
					Prefix(Token.UInt16);
					Serialize((UInt16)value);
					break;
				case TypeCode.UInt32:
					Prefix(Token.UInt32);
					Serialize((UInt32)value);
					break;
				case TypeCode.UInt64:
					Prefix(Token.UInt64);
					Serialize((UInt64)value);
					break;
				default:
					throw new ArgumentException("Unknown primitive type: " + type);
			}
		}
		
		/// <summary>
		/// Dispose of this instance.
		/// </summary>
		/// <remarks>
		/// This will not close the stream if <see cref="KeepOpen"/> is set to true.
		/// </remarks>
		public void Dispose() {
			if(Open) {
				if(!KeepOpen)
					stream.Close();
				stream = null;
			}
		}

		#endregion

		#region Constructor
		
		/// <summary>
		/// Default Ctor.
		/// </summary>
		public Serializer() {	}
		
		/// <summary>
		/// Contstruct an instance around the given stream, using the given settings.
		/// </summary>
		public Serializer(Stream stream, IContractor contractor = null, AliasBinder binder = null, int bufferSize = 256) {
			if(null == stream)
				throw new ArgumentNullException("stream");
			if(!stream.CanWrite)
				throw new ArgumentException("Cannot construct instance from non-writeable stream", "stream");
			this.stream = stream;
			session = new SerializerSession(contractor: contractor, aliasBinder: binder);
			serializeStack = new Stack<object>();
			if(bufferSize < 256)
				throw new ArgumentOutOfRangeException("bufferSize", "Minimum buffer size is 256.");
			buffer = new byte[bufferSize];
		}

		/// <summary>
		/// Construct an instance around the given Stream, using the given session
		/// settings.
		/// </summary>
		public Serializer(Stream stream, SerializerSession session, int bufferSize = 256) {
			if(null == stream)
				throw new ArgumentNullException("stream");
			if(!stream.CanWrite)
				throw new ArgumentException("Cannot construct instance from non-writeable stream", "stream");
			this.stream = stream;
			this.session = session ?? throw new ArgumentNullException("session");
			serializeStack = new Stack<object>();
			if(bufferSize < 256)
				throw new ArgumentOutOfRangeException("bufferSize", "Minimum buffer size is 256.");
			buffer = new byte[bufferSize];
		}
		
		/// <summary>
		/// Constructs an instance that serializes to the file at the given
		/// path, using the given settings.
		/// </summary>
		public Serializer(string filepath, IContractor contractor = null, AliasBinder binder = null, int bufferSize = 256) {
			stream = File.Create(filepath ?? throw new ArgumentNullException("filepath"));
			session = new SerializerSession(contractor: contractor, aliasBinder: binder);
			serializeStack = new Stack<object>();
			if(bufferSize < 256)
				throw new ArgumentOutOfRangeException("bufferSize", "Minimum buffer size is 256.");
			buffer = new byte[bufferSize];
		}
		
		/// <summary>
		/// Construct an instance that serializes to the file at the given path, using
		/// the given session settings.
		/// </summary>
		public Serializer(string filepath, SerializerSession session, int bufferSize = 256) {
			stream = File.Create(filepath ?? throw new ArgumentNullException("filepath"));
			this.session = session ?? throw new ArgumentNullException("session");
			serializeStack = new Stack<object>();
			if(bufferSize < 256)
				throw new ArgumentOutOfRangeException("bufferSize", "Minimum buffer size is 256.");
			buffer = new byte[bufferSize];
		}

		#endregion

		#region Fields

		private Stream stream;
		private readonly SerializerSession session;
		private readonly Stack<object> serializeStack;
		private byte[] buffer;
		private int pending;

		private readonly static string disposedStreamMessage = "Stream is null; instance has been disposed.";

		#endregion

		/// <summary>
		/// Exploits struct layout tricks to allow copying a Guid directly to a
		/// pre-existing buffer.
		/// </summary>
		[StructLayout(LayoutKind.Explicit)]
		private struct GuidCopier {
			#region Methods

			/// <summary>
			/// Copies the Guid to the given buffer, in the same format as produced by
			/// Guid.ToByteArray.
			/// </summary>
			public void CopyTo(byte[] buffer, int offset) {
				if(null == buffer)
					throw new ArgumentNullException("buffer");
				if(buffer.Length - offset < 16)
					throw new ArgumentException("Insufficient buffer space.");

				buffer[offset] = (byte)a;
				buffer[++offset] = (byte)(a >> 8);
				buffer[++offset] = (byte)(a >> 16);
				buffer[++offset] = (byte)(a >> 24);
				buffer[++offset] = (byte)b;
				buffer[++offset] = (byte)(b >> 8);
				buffer[++offset] = (byte)c;
				buffer[++offset] = (byte)(c >> 8);
				buffer[++offset] = d;
				buffer[++offset] = e;
				buffer[++offset] = f;
				buffer[++offset] = g;
				buffer[++offset] = h;
				buffer[++offset] = i;
				buffer[++offset] = j;
				buffer[++offset] = k;
			}

			/// <summary>
			/// Create an instance that holds the given Guid.
			/// </summary>
			public static GuidCopier Create(Guid id) {
				return new GuidCopier {
					guid = id
				};
			}

			#endregion

			#region Fields

			[FieldOffset(0)]
			public Guid guid;
			// These fields overlap the Guid's instance fields.
			[FieldOffset(0)]
			private int a;
			[FieldOffset(4)]
			private short b;
			[FieldOffset(6)]
			private short c;
			[FieldOffset(8)]
			private byte d;
			[FieldOffset(9)]
			private byte e;
			[FieldOffset(10)]
			private byte f;
			[FieldOffset(11)]
			private byte g;
			[FieldOffset(12)]
			private byte h;
			[FieldOffset(13)]
			private byte i;
			[FieldOffset(14)]
			private byte j;
			[FieldOffset(15)]
			private byte k;

			#endregion
		}

		/// <summary>
		/// Exploits struct layout trickery to facilitate easier copying to
		/// a buffer.
		/// </summary>
		[StructLayout(LayoutKind.Explicit)]
		private struct DecimalCopier {
			#region Properties

			/// <summary>
			/// Returns true if the isNegative flag is set.
			/// </summary>
			public bool IsNegative {
				get {	return (flags & signMask) == 1;	}
			}

			/// <summary>
			/// Returns the scale value from the flags field.
			/// </summary>
			public byte Scale {
				get { return (byte)((flags & scaleMask) >> 16); }
			}

			#endregion

			#region Methods

			/// <summary>
			/// Copies this value to the given buffer. This method omits the
			/// unused bytes of the flags field, resulting in a 14-byte size.
			/// </summary>
			public void CopyTo(byte[] buffer, int offset) {
				if(null == buffer)
					throw new ArgumentNullException("buffer");
				if(buffer.Length - offset < 14)
					throw new ArgumentException("Insufficient buffer space.");

				buffer[offset] = (byte)lo;
				buffer[++offset] = (byte)(lo >> 8);
				buffer[++offset] = (byte)(lo >> 16);
				buffer[++offset] = (byte)(lo >> 24);

				buffer[++offset] = (byte)mid;
				buffer[++offset] = (byte)(mid >> 8);
				buffer[++offset] = (byte)(mid >> 16);
				buffer[++offset] = (byte)(mid >> 24);

				buffer[++offset] = (byte)hi;
				buffer[++offset] = (byte)(hi >> 8);
				buffer[++offset] = (byte)(hi >> 16);
				buffer[++offset] = (byte)(hi >> 24);

				buffer[++offset] = (byte)(IsNegative ? 1 : 0);
				buffer[++offset] = Scale;
				
			}

			/// <summary>
			/// Create a value from the given decimal.
			/// </summary>
			public static DecimalCopier Create(decimal dec) {
				return new DecimalCopier {
					dec = dec
				};
			}

			#endregion
			
			#region Fields

			[FieldOffset(0)]
			public decimal dec;

			[FieldOffset(0)]
			private int flags;
			[FieldOffset(4)]
			private int hi;
			[FieldOffset(8)]
			private int lo;
			[FieldOffset(12)]
			private int mid;

			private const int signMask = unchecked((int)0x80000000);
			private const int scaleMask = 0x00FF0000;

			
			#endregion
		}
	}
}
