﻿namespace SurlyWyvern.Herald {
	/// <summary>
	/// Denotes how a <see cref="Serializer"/> instance handles a 
	/// reference loop when <see cref="Serializer.ReferenceHandling"/> is set to
	/// false.
	/// </summary>
	public enum ReferenceLoopHandling {
		/// <summary>
		/// Throw a SerializationException.
		/// </summary> 
		Exception = 1,
		/// <summary>
		///  Serialize a null value instead.
		/// </summary>
		Nullify = 2,
	}
}
