﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Deserializes data from a stream that was serialized using
	/// <see cref="Serializer"/>.
	/// </summary>
	public class Deserializer : IDisposable {
		#region Properties

		/// <summary>
		/// If true, underlying Stream is not closed when this instance is disposed.
		/// </summary>
		public bool KeepOpen {
			get;
			set;
		}

		/// <summary>
		/// Returns true if the underlying stream can be read from.
		/// </summary>
		public bool Open {
			get { return null != stream; }
		}

		/// <summary>
		/// Contractor used to get contracts for deserialized objects.
		/// </summary>
		/// <remarks>
		/// This Contractor should use the same settings as the Contractor
		/// used for serialization.
		/// </remarks>
		public IContractor Contractor {
			get;
		}

		/// <summary>
		/// Binds types to aliased names.
		/// </summary>
		/// <remarks>
		/// This Binder should use the same settings as the instance used to serialize
		/// the data.
		/// </remarks>
		public AliasBinder Binder {
			get;
		}
		
		public Encoding Encoding {
			get;
		} = Encoding.UTF8;

		#endregion

		#region Methods
		
		/// <summary>
		/// Deserialize the next object from the stream.
		/// </summary>
		/// <remarks>
		/// This method should be used to deserialize data that was serialized
		/// using <see cref="Serializer.SerializeExplicit"/>, as it requires the Token
		/// prefix for all types, and the type prefix for structs and classes.
		/// </remarks>
		public object Deserialize() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			
			var token = ReadToken();
			if(Token.Null == token)
				return null;
			
			Type type = null;
			int reference = -1;
			object result = null;
			switch(token) {
				case Token.Ref:
					reference = ReadInt32();
					result = dereferencedObjects[reference];
					break;
				case Token.Boolean:
					return ReadBoolean();
				case Token.Byte:
					return ReadByte();
				case Token.Int16:
					return ReadInt16();
				case Token.Int32:
					return ReadInt32();
				case Token.Int64:
					return ReadInt64();
				case Token.Float32:
					return ReadSingle();
				case Token.Float64:
					return ReadDouble();
				case Token.SByte:
					return ReadSByte();
				case Token.Char:
					return ReadChar();
				case Token.UInt16:
					return ReadUInt16();
				case Token.UInt32:
					return ReadUInt32();
				case Token.UInt64:
					return ReadUInt64();
				case Token.Decimal:
					return ReadDecimal();
				case Token.DateTime:
					return ReadDateTime();
				case Token.TimeSpan:
					return ReadTimeSpan();
				case Token.DateTimeOffset:
					return ReadDateTimeOffset();
				case Token.Guid:
					return ReadGuid();
				case Token.String:
					return ReadString();
				case Token.StringRef:
					reference = ReadInt32();
					result = ReadString();
					dereferencedObjects.Add(reference, result);
					return result;
				case Token.Type:
					return Binder.BindToType(ReadString());
				case Token.TypeRef:
					reference = ReadInt32();
					result = Binder.BindToType(ReadString());
					if(null != result)
						dereferencedObjects.Add(reference, result);
					return result;
				case Token.Enum:
					type = (Type)Deserialize();
					result = Enum.ToObject(type, Deserialize(Enum.GetUnderlyingType(type)));
					return result;
				case Token.Struct:
					type = (Type)Deserialize();
					if(SelfContractType.IsAssignableFrom(type)) {
						result = CreateInstance(type);
						((ISelfContract)result).Read(this);
					} else {
						result = Contractor.GetContract(token, type).Read(this, type);
					}
					break;
				case Token.ArrayRef:
					reference = ReadInt32();
					type = (Type)Deserialize();
					int length = ReadInt32();
					result = Array.CreateInstance(type, length);
					dereferencedObjects.Add(reference, result);
					if(type.IsPrimitive)
						ReadArray((Array)result);
					else
						result = Contractor.GetContract(token, type).Read(this, type, result);
					break;
				case Token.Array:
					type = (Type)Deserialize();
					length = ReadInt32();
					result = Array.CreateInstance(type, length);
					if(type.IsPrimitive)
						ReadArray((Array)result);
					else
						result = Contractor.GetContract(token, type).Read(this, type, result);
					break;
				case Token.ObjectRef:
					reference = ReadInt32();
					type = (Type)Deserialize();
					result = CreateInstance(type);
					dereferencedObjects.Add(reference, result);
					if(SelfContractType.IsAssignableFrom(type))
						((ISelfContract)result).Read(this);
					else
						result = Contractor.GetContract(token, type).Read(this, type, result);
					break;
				case Token.Object:
					type = (Type)Deserialize();
					result = CreateInstance(type);
					if(SelfContractType.IsAssignableFrom(type))
						((ISelfContract)result).Read(this);
					else
						result = Contractor.GetContract(token, type).Read(this, type, result);
					break;
				default:
					throw new IOException("Unknown token read at position " + stream.Position + ": " + token);
			}
			return result;
		}
		
		/// <summary>
		/// Deserialize an object that was serialized using
		/// <see cref="Serializer.Serialize"/>.
		/// </summary>
		/// <remarks>
		/// The type provided must be the EXACT type of the object that was
		/// serialized - NOT a base class or interface.
		/// </remarks>
		/// <param name="type">The exact type that was serialized.</param>
		public object Deserialize(Type type) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			if(null == type)
				throw new ArgumentNullException("type");
			if(type.IsAbstract && typeof(Type) != type)
				throw new ArgumentException("Cannot explicitly deserialize an abstract class or interface; a concrete type is required.", "type");
			if(type.IsGenericTypeDefinition)
				throw new ArgumentException("Cannot explicitly deserialize a generic type definition; a concrete type is required.", "type");

			object result;
			int reference;
			
			if(type.IsPrimitive) {
				switch(Type.GetTypeCode(type)) {
					case TypeCode.Boolean:
						return ReadBoolean();
					case TypeCode.Byte:
						return ReadByte();
					case TypeCode.Int16:
						return ReadInt16();
					case TypeCode.Int32:
						return ReadInt32();
					case TypeCode.Int64:
						return ReadInt64();
					case TypeCode.Single:
						return ReadSingle();
					case TypeCode.Double:
						return ReadDouble();
					case TypeCode.SByte:
						return ReadSByte();
					case TypeCode.Char:
						return ReadChar();
					case TypeCode.UInt16:
						return ReadUInt16();
					case TypeCode.UInt32:
						return ReadUInt32();
					case TypeCode.UInt64:
						return ReadUInt64();
					default:
						throw new ArgumentException("Unknown primitive type: " + type, "type");
				}
			} else if(type == typeof(decimal)) {
				return ReadDecimal();
			} else if(type == typeof(DateTime)) {
				return ReadDateTime();
			} else if(type == typeof(TimeSpan)) {
				return ReadTimeSpan();
			} else if(type == typeof(DateTimeOffset)) {
				return ReadDateTimeOffset();
			} else if(type == typeof(Guid)) {
				return ReadGuid();
			} else if(type.IsEnum) {
				return Enum.ToObject(type, Deserialize(Enum.GetUnderlyingType(type)));
			} else if(Nullable.GetUnderlyingType(type) != null) {
				if(ReadToken() == Token.Null)
					return null;
				else
					return Deserialize(Nullable.GetUnderlyingType(type));
			}	else if(type.IsValueType) {
				if(SelfContractType.IsAssignableFrom(type)) {
					result = CreateInstance(type);
					((ISelfContract)result).Read(this);
				} else {
					result = Contractor.GetContract(type).Read(this, type);
				}
				return result;
			}
			// All other types prefix a token.
			var token = ReadToken();
			switch(token) {
				case Token.Null:
					return null;
				case Token.Ref:
					int r = ReadInt32();
					if(!dereferencedObjects.TryGetValue(r, out result))
						throw new KeyNotFoundException("No dereferenced object with ID: " + r);
					if(result.GetType() != type && (type != typeof(Type) || !type.IsAssignableFrom(result.GetType())))
						throw new ArgumentException("Referenced object type (" + result.GetType() + ") did not match provided type. (" + type + ")", "type");
					return result;
				case Token.StringRef:
					if(type != typeof(string))
						throw new ArgumentException("Read StringRef token, but desired type was: " + type, "type");
					reference = ReadInt32();
					result = ReadString();
					dereferencedObjects.Add(reference, result);
					return result;
				case Token.String:
					if(type != typeof(string))
						throw new ArgumentException("Read String token, but desired type was: " + type, "type");
					return ReadString();
				case Token.TypeRef:
					if(!typeof(Type).IsAssignableFrom(type))
						throw new ArgumentException("Read TypeRef token, but desired type was: " + type, "type");
					reference = ReadInt32();
					result = Binder.BindToType(ReadString());
					if(null != result)
						dereferencedObjects.Add(reference, result);
					return result;
				case Token.Type:
					if(!typeof(Type).IsAssignableFrom(type))
						throw new ArgumentException("Read Type token, but desired type was: " + type, "type");
					return Binder.BindToType(ReadString());
				case Token.ArrayRef:
					if(!type.IsArray)
						throw new ArgumentException("Read ArrayRef token, but desired type was not an array: " + type, "type");
					reference = ReadInt32();
					int length = ReadInt32();
					result = Array.CreateInstance(type.GetElementType(), length);
					dereferencedObjects.Add(reference, result);
					if(type.GetElementType().IsPrimitive)
						ReadArray((Array)result);
					else
						Contractor.GetContract(type).Read(this, type.GetElementType(), result);
					return result;
				case Token.Array:
					if(!type.IsArray)
						throw new ArgumentException("Read Array token, but desired type was not an array: " + type, "type");
					length = ReadInt32();
					result = Array.CreateInstance(type.GetElementType(), length);
					if(type.GetElementType().IsPrimitive)
						ReadArray((Array)result);
					else
						Contractor.GetContract(type).Read(this, type.GetElementType(), result);
					return result;
				case Token.ObjectRef:
					if(type.IsValueType || type.IsArray || type == typeof(string) || type == typeof(Type))
						throw new ArgumentException("Read ObjectRef token, but desired type was not an object: " + type, "type");
					reference = ReadInt32();
					result = CreateInstance(type);
					dereferencedObjects.Add(reference, result);
					if(SelfContractType.IsAssignableFrom(type))
						((ISelfContract)result).Read(this);
					else
						result = Contractor.GetContract(type).Read(this, type, result);
					return result;
				case Token.Object:
					if(type.IsValueType || type.IsArray || type == typeof(string) || type == typeof(Type))
						throw new ArgumentException("Read Object token, but desired type was not an object: " + type, "type");
					result = CreateInstance(type);
					if(SelfContractType.IsAssignableFrom(type))
						((ISelfContract)result).Read(this);
					else
						result = Contractor.GetContract(type).Read(this, type, result);
					return result;
				default:
					throw new IOException("Read the following token: " + token + ", but this is not valid within Deserialize(Type)");
			}
		}
		
		/// <summary>
		/// Deserialize a polymorphic value that was serialized via 
		/// <see cref="Serializer.Serialize(object, Type)"/>.
		/// </summary>
		/// <param name="containerType">
		/// Base class or interface that is being deserialized.
		/// </param>
		public object DeserializePoly(Type containerType) {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			if(null == containerType)
				throw new ArgumentNullException("containerType");
			if(containerType.IsSealed)
				throw new ArgumentException("DeserializePoly can only be used with non-sealed types.", "containerType");
			if(typeof(object) == containerType)
				return Deserialize();

			var token = ReadByte();
			if((byte)PolyToken.Null == token)
				return null;
			else if((byte)PolyToken.Instance == token) {
				return Deserialize(containerType);
			} else {
				var type = (Type)Deserialize();
				return Deserialize(type);
			}
		}

		/// <summary>
		/// Generic overload for cleaner code.
		/// </summary>
		public T Deserialize<T>() {
			return (T)Deserialize(typeof(T));
		}

		/// <summary>
		/// Generic overload for cleaner code.
		/// </summary>
		public T DeserializePoly<T>() {
			return (T)DeserializePoly(typeof(T));
		}

		#region Stream Reading
		
		private Token ReadToken() {
			return (Token)ReadByte();
		}
		
		public bool ReadBoolean() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			return ReadByte() != 0;
		}
		
		public byte ReadByte() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			var result = stream.ReadByte();
			if(result < 0)
				throw new EndOfStreamException("End of Stream reached.");
			return (byte)result;
		}
		
		public ushort ReadUInt16() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(2);
			return (ushort)(buffer[0] | buffer[1] << 8);
		}

		public uint ReadUInt32() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(4);
			return (uint)(buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24);
		}

		public ulong ReadUInt64() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(8);
			uint lower = (uint)(buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24);
			uint higher = (uint)(buffer[4] | buffer[5] << 8 | buffer[6] << 16 | buffer[7] << 24);
			return higher << 32 | (ulong)lower;
		}

		public sbyte ReadSByte() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			return (sbyte)ReadByte();
		}

		public short ReadInt16() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(2);
			return (short)(buffer[0] | buffer[1] << 8);
		}

		public int ReadInt32() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(4);
			return buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24;
		}

		public long ReadInt64() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(8);
			uint lower = (uint)(buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24);
			uint higher = (uint)(buffer[4] | buffer[5] << 8 | buffer[6] << 16 | buffer[7] << 24);
			return (long)((ulong)higher << 32 | lower);
		}

		public unsafe float ReadSingle() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			int temp = ReadInt32();
			return *(float*)&temp;
		}

		public unsafe double ReadDouble() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(8);
			uint lower = (uint)(buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24);
			uint higher = (uint)(buffer[4] | buffer[5] << 8 | buffer[6] << 16 | buffer[7] << 24);
			ulong temp = (ulong)higher << 32 | lower;
			return *(double*)&temp;
		}

		public unsafe char ReadChar() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			int byteCount = Encoding.GetMaxByteCount(1);
			if(byteCount == 1) {
				buffer[0] = ReadByte();
				Encoding.GetChars(buffer, 0, 1, charBuffer, 0);
				return charBuffer[0];
			} else {
				var readCount = stream.Read(buffer, 0, byteCount);
				Encoding.GetChars(buffer, 0, readCount, charBuffer, 0);
				return charBuffer[0];
			}
		}

		public decimal ReadDecimal() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(14);
			int lo = buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24;
			int mid = buffer[4] | buffer[5] << 8 | buffer[6] << 16 | buffer[7] << 24;
			int hi = buffer[8] | buffer[9] << 8 | buffer[10] << 16 | buffer[11] << 24;
			
			return new Decimal(lo, mid, hi, buffer[12] == 1, buffer[13]);
		}

		public DateTime ReadDateTime() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(9);
			uint lowerTicks = (uint)(buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24);
			uint higherTicks = (uint)(buffer[4] | buffer[5] << 8 | buffer[6] << 16 | buffer[7] << 24);

			return new DateTime((long)higherTicks << 32 | lowerTicks, (DateTimeKind)buffer[8]);
		}

		public TimeSpan ReadTimeSpan() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			return new TimeSpan(ReadInt64());
		}

		public DateTimeOffset ReadDateTimeOffset() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			ToBuffer(10);

			uint lowerTicks = (uint)(buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24);
			uint higherTicks = (uint)(buffer[4] | buffer[5] << 8 | buffer[6] << 16 | buffer[7] << 24);
			long ticks = (long)((ulong)higherTicks << 32 | lowerTicks);
			short mins = (short)(buffer[8] | buffer[9] << 8);
			return new DateTimeOffset(new DateTime(ticks, DateTimeKind.Unspecified),
				TimeSpan.FromMinutes(mins));
			
		}

		public Guid ReadGuid() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			stream.Read(guidBuffer, 0, 16);
			return new Guid(guidBuffer);
		}
		
		public int ReadBytes(byte[] buffer, int offset, int count) {
			if(null == buffer)
				throw new ArgumentNullException("buffer");
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			if(count < 0)
				throw new ArgumentOutOfRangeException("count");
			if(offset + count > buffer.Length)
				throw new ArgumentException("Read operation would overflow buffer. (" + offset + "+" + count + " > " + buffer.Length + ")");

			return stream.Read(buffer, offset, count);
		}

		private string ReadString() {
			if(!Open)
				throw new ObjectDisposedException("stream", disposedStreamMessage);
			// Read string length.
			int byteLength = 0;
			{
				byte readByte = 0;
				int bitCount = 0;
				do {
					if(bitCount > 35)
						throw new FormatException("Length prefix of string cannot exceed 5 bytes in size.");
					readByte = ReadByte();
					byteLength |= (readByte & 0x7F) << bitCount;
					bitCount += 7;

				} while((readByte & 0x80) != 0);
			}

			if(byteLength == 0)
				return string.Empty;
			if(byteLength <= buffer.Length) {
				ToBuffer(byteLength);
				var charCount = Encoding.GetChars(buffer, 0, byteLength, charBuffer, 0);
				return new string(charBuffer, 0, charCount);
			} else {
				int bytesRead = 0;
				while(bytesRead < byteLength) {
					var maxRead = (byteLength - bytesRead) > buffer.Length ? buffer.Length : (byteLength - bytesRead);
					var read = stream.Read(buffer, 0, maxRead);
					if(0 == read)
						throw new EndOfStreamException("Unexpected end of stream while reading string.");
					bytesRead += read;
					var charsRead = Encoding.GetChars(buffer, 0, read, charBuffer, 0);
					
					stringBuilder.Append(charBuffer, 0, charsRead);
				}

				var result = stringBuilder.ToString();
				stringBuilder.Length = 0;
				return result;
			}
		}

		private void ReadArray(Array array) {
			int length = Buffer.ByteLength(array);
			int progress = 0;
			while(progress < length) {
				int copyCount = length - progress;
				if(copyCount > buffer.Length)
					copyCount = buffer.Length;
				stream.Read(buffer, 0, copyCount);
				Buffer.BlockCopy(buffer, 0, array, progress, copyCount);
				progress += copyCount;
			}
		}

		private void ToBuffer(int bytes) {
			if(bytes > buffer.Length)
				throw new ArgumentOutOfRangeException("bytes", "Desired byte count exceeds buffer size.");

			if(bytes != stream.Read(buffer, 0, bytes))
				throw new EndOfStreamException("Unexpected end of stream.");
		}

		#endregion

		/// <summary>
		/// Dispose of this instance.
		/// </summary>
		/// <remarks>
		/// This will not close the stream if <see cref="KeepOpen"/> is set to true.
		/// </remarks>
		public void Dispose() {
			if(Open) {
				if(!KeepOpen)
					stream.Close();
				stream = null;
			}
		}

		#endregion

		#region Constructors
		
		/// <summary>
		/// Default Ctor.
		/// </summary>
		public Deserializer() { }
		
		/// <summary>
		/// Contstruct an instance around the given stream, using the given settings.
		/// </summary>
		public Deserializer(Stream stream, IContractor contractor = null, AliasBinder binder = null, bool keepOpen = false, int bufferSize = 256) {
			this.stream = stream ?? throw new ArgumentNullException("stream");
			if(!stream.CanRead)
				throw new ArgumentException("Cannot construct instance from un-readable stream", "stream");
			Contractor = contractor ?? Herald.Contractor.Default;
			Binder = binder ?? AliasBinder.DefaultInstance;
			KeepOpen = keepOpen;
			if(bufferSize < 256)
				throw new ArgumentOutOfRangeException("bufferSize", "Minimum buffer size is 256.");
			buffer = new byte[bufferSize];
			charBuffer = new char[Encoding.GetMaxCharCount(buffer.Length)];
			guidBuffer = new byte[16];
			stringBuilder = new StringBuilder(charBuffer.Length);
		}
		
		/// <summary>
		/// Construct an instance to deserialize the file at the given path, using
		/// the given settings.
		/// </summary>
		public Deserializer(string filepath, IContractor contractor = null, AliasBinder binder = null, int bufferSize = 256) {
			if(null == filepath)
				throw new ArgumentNullException("filepath");
			if(bufferSize < 256)
				throw new ArgumentOutOfRangeException("bufferSize", "Minimum buffer size is 256.");
			buffer = new byte[bufferSize];
			charBuffer = new char[Encoding.GetMaxCharCount(buffer.Length)];
			guidBuffer = new byte[16];
			stringBuilder = new StringBuilder(charBuffer.Length);
			stream = File.OpenRead(filepath);
			Contractor = contractor ?? Herald.Contractor.Default;
			Binder = binder ?? AliasBinder.DefaultInstance;
			KeepOpen = false;
		}

		#endregion

		#region Static Methods

		/// <summary>
		/// Creates an instance of the given class.
		/// </summary>
		public static T CreateInstance<T>() {
			return (T)CreateInstance(typeof(T));
		}

		/// <summary>
		/// Creates an instance of the given class.
		/// </summary>
		/// <remarks>
		/// The type is created via its default constructor if it exists, or an
		/// uninitialized object is created.
		/// </remarks>
		public static object CreateInstance(Type type) {
			if(null == type)
				throw new ArgumentNullException("type");
			if(type.IsAbstract)
				throw new ArgumentException("type", "Cannot instantiate an abstract class or interface.");
			if(type.IsGenericTypeDefinition)
				throw new ArgumentException("type", "Cannot instantiate a generic type definition.");
			if(type.IsValueType)
				return Activator.CreateInstance(type, true);
			var ctor = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
			return ctor is null ? FormatterServices.GetUninitializedObject(type) : ctor.Invoke(null);
		}

		#endregion

		#region Fields

		private Stream stream;
		private readonly Dictionary<int, object> dereferencedObjects = new Dictionary<int, object>();
		private readonly byte[] buffer;
		private readonly byte[] guidBuffer;
		private readonly char[] charBuffer;
		private readonly StringBuilder stringBuilder;

		private readonly static Type SelfContractType = typeof(ISelfContract);
		private readonly static string disposedStreamMessage = "Stream is null; instance has been disposed.";

		#endregion

	}
}
