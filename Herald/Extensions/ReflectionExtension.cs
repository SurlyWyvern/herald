﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace SurlyWyvern.Herald.Extensions {
	public static class ReflectionExtension {
		/// <summary>
		/// Get all fields of the given type, including private fields declared in
		/// base classes.
		/// </summary>
		public static FieldInfo[] GetAllFields(this Type type, BindingFlags bindingFlags) {
			var result = new List<FieldInfo>();
			// Force DeclaredOnly on, otherwise we will have duplicates of inherited fields.
			bindingFlags |= BindingFlags.DeclaredOnly;
			while(null != type) {
				result.AddRange(type.GetFields(bindingFlags));
				type = type.BaseType;
			}
			return result.ToArray();
		}

		/// <summary>
		/// Get all properties of the given type, including private properties
		/// declared in base classes.
		/// </summary>
		public static PropertyInfo[] GetAllProperties(this Type type, BindingFlags bindingFlags) {
			var result = new List<PropertyInfo>();
			bindingFlags |= BindingFlags.DeclaredOnly;
			while(null != type) {
				result.AddRange(type.GetProperties(bindingFlags));
				type = type.BaseType;
			}
			return result.ToArray();
		}


	}
}
