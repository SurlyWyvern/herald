﻿namespace SurlyWyvern.Herald {
	/// <summary>
	/// Single byte tokens prefixed to serialized data.
	/// </summary>
	public enum Token : byte {
		Null = 0,
		// Primitives
		Boolean = 1,
		Byte = 2,
		Int16 = 3,
		Int32 = 4,
		Int64 = 5,
		Float32 = 6,
		Float64 = 7,
		SByte = 8,
		Char = 9,
		UInt16 = 10,
		UInt32 = 11,
		UInt64 = 12,
		// Built-in structs and classes.
		Decimal = 20,
		Guid = 21,
		DateTime = 22,
		TimeSpan = 23,
		DateTimeOffset = 24,
		Enum = 25,
		String = 26,
		StringRef = 27,
		Type = 28,
		TypeRef = 29,
		// Non-builtin types.
		Object = 170,
		ObjectRef = 171,
		Array = 186,
		ArrayRef = 187,
		Struct = 202,
		// Ref is used for subsequent appearances of reference values in the stream.
		// Initial instances are denoted by the -Ref tokens, such as ArrayRef.
		Ref = 218,
	}

}
