﻿namespace SurlyWyvern.Herald {
	/// <summary>
	/// Single byte tokens prefixed to elements of polymorphic collections.
	/// </summary>
	/// <remarks>
	/// The Null prefix denotes that a null reference was serialized.
	/// The Instance prefix denotes that an instance of the container's type follows,
	/// with no type specified.
	/// The Inheritor prefix denotes that a subclass of the container's type follows,
	/// preceded by its exact type.
	/// </remarks>
	public enum PolyToken : byte {
		Null = 0,
		Instance = 1,
		Inheritor = 2,
	}

}
