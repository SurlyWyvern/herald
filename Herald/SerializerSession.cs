﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SurlyWyvern.Herald {
	/// <summary>
	/// Holds persistent settings for a serialization session, allowing data to be
	/// serialized over multiple <see cref="Serializer"/> instances.
	/// </summary>
	public class SerializerSession {
		#region Properties

		/// <summary>
		/// If true, reference type instances will be associated with a 4-byte
		/// reference ID. Subsequent appearances of any given instance will be 
		/// replaced with the ID.
		/// </summary>
		public bool ReferenceHandling {
			get;
		}
		
		/// <summary>
		/// Defines the handling of reference loops for this session.
		/// </summary>
		public ReferenceLoopHandling ReferenceLoopHandling {
			get;
		}

		/// <summary>
		/// <see cref="Contractor"/> used for this session.
		/// </summary>
		public IContractor Contractor {
			get;
		}

		/// <summary>
		/// <see cref="AliasBinder"/> used for this session.
		/// </summary>
		public AliasBinder AliasBinder {
			get;
		}
		
		/// <summary>
		/// Counts the number of referenced objects serialized during this session.
		/// </summary>
		public int RefCounter {
			get;
			private set;
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Checks whether the given object has been referenced in this session.
		/// </summary>
		public bool IsReferenced(object value) {
			if(null == value)
				throw new ArgumentNullException("value");
			return referenceMap.ContainsKey(value);
		}

		/// <summary>
		/// Gets the reference ID for the given object.
		/// </summary>
		public int GetReferenceID(object value) {
			return referenceMap[value];
		}
		
		/// <summary>
		/// Adds the given object to the reference map.
		/// </summary>
		/// <returns>The object's associated reference ID.</returns>
		public int MakeReference(object value) {
			referenceMap.Add(value, ++RefCounter);
			return RefCounter;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Default ctor.
		/// </summary>
		public SerializerSession() {
			ReferenceHandling = true;
			ReferenceLoopHandling = ReferenceLoopHandling.Nullify;
			Contractor = Herald.Contractor.Default;
			AliasBinder = AliasBinder.DefaultInstance;
			referenceMap = new Dictionary<object, int>();
			RefCounter = 0;
		}
		
		/// <summary>
		/// Explicit ctor.
		/// </summary>
		public SerializerSession(bool referenceHandling = true,
				ReferenceLoopHandling referenceLoopHandling = ReferenceLoopHandling.Nullify,
				IContractor contractor = null,
				AliasBinder aliasBinder = null,
				Dictionary<object, int> refMap = null) {
			ReferenceHandling = referenceHandling;
			ReferenceLoopHandling = referenceLoopHandling;
			Contractor = contractor ?? Herald.Contractor.Default;
			AliasBinder = aliasBinder ?? AliasBinder.DefaultInstance;
			referenceMap = refMap ?? new Dictionary<object, int>();
			RefCounter = null == refMap ? 0 : refMap.Values.Max();
		}

		#endregion

		#region Fields

		private readonly Dictionary<object, int> referenceMap;

		#endregion
	}
}
