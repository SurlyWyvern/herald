# Herald README #

Herald is a binary-formatted serializer written in C#. It is
intended primarily for working with files.

---

## Usage ##

Data is serialized via an instance of the `Serializer` class, And deserialized
via an instance of the `Deserializer` class.

There are three ways to serialize data with Herald.

Firstly, there are the 'normal' `Serialize` methods, that take only the 
value to be serialized as an argument:

    using(var serializer = new Serializer(stream)) {
        serializer.Serialize(1842);
    }

This is the most efficient serialization, but requires that the exact type
of the data be provided on deserialization:

    using(var deserializer = new Deserializer(stream)) {
        result = deserializer.Deserialize<int>();
    }

Secondly, there is the `SerializePoly` method, that takes the value to be
serialized and a 'container type' as arguments. The container type may be either
the type of the value, or a class it inherits or interface it implements.

    using(var serializer = new Serializer(stream)) {
        serializer.SerializePoly(aSubClassInstance, typeof(SuperClass));
    }

In cases where the provided value is a subclass or implementation of the container type,
type information will be included in the serialization to allow it to be deserialized.
When deserialized via `DeserializePoly`, the correct container type must be provided:

    using(var deserializer = new Deserializer(stream)) {
        deserializer.DeserializePoly(typeof(SuperClass));
    }

Finally, there is explicit serialization:

    using(var serializer = new Serializer(stream)) {
        serializer.SerializeExplicit(anyObject);
    }

Explicit serialization accepts any type and always includes type information, allowing
the value to be deserialized without knowing its type:

    using(var deserializer = new Deserializer(stream)) {
        result = deserializer.Deserialize();
    }

Herald provides out-of-the-box serialization for the following types:

* All primitive types
* Guid
* DateTime
* TimeSpan
* DateTimeOffset
* Decimal
* Nullable types for all of the above
* Strings
* Single-dimensional arrays
* ICollection<T> implementations, including List<T>, HashSet<T>, and Dictionary<K, V>

Other types are serialized via 'contracts'.

### Contractors and Contracts ###

`Serializer` and `Deserializer` instances use implementations of the `IContractor`
interface to control the assignment of `IContract` implementations to different
types to control their serialization and deserialization.

The default `IContractor` implementation is the `Contractor` class. It uses the
`ILContract` class to create contracts at runtime.

If no `IContractor` is defined for a `Serializer` or `Deserializer`, they will use
`Contractor.Default`.

#### ILContract ####

`ILContract` is the default type of contract generated for classes and structs
that do not have a built-in or custom contract defined.

`ILContract` uses Intermediate Language emission to create dynamic methods that
serialize and deserialize the members of a type. By default, `ILContract` serializes
a type's fields, including private fields inherited from a base class. The members
that are automatically included for serialization can be customised by providing a
`MemberInclusion` argument to a `Contractor`'s constructor:

	// This Contractor's ILContracts will only serialize a type's properties.
	var contractor = new Contractor(MemberInclusion.PublicProperties | MemberInclusion.NonPublicProperties);
	using(var serializer = new Serializer(stream, contractor)) {
		serializer.Serialize(anObjectThatHasProperties);
	}

    // Data must be deserialized using a Contractor with the same settings.
	using(var deserializer = new Deserializer(stream, contractor)) {
		result = deserializer.Deserialize<ATypeThatHasProperties>();
	}
		
If you wish to create a custom contract without writing an entire class, you can
manually construct an `ILContract` instance and define the members it should
serialize:

	// This ILContract will only serialize this type's private fields.
	var fields = typeof(MyClass).GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
	var contract = new ILContract(typeof(MyClass), fields, null);
    // This Contractor will use the created contract to serialize MyClass
    var contractor = new Contractor(contract);

#### Custom Contracts ####

If auto-generated contracts do not suffice, Herald provides the means to define
custom contracts that control serialization for their assigned types.

`Contractor` instances can be provided with implementations of the `IContract`
and `IContractFactory` interfaces upon construction to define custom
serialization behaviour for specific classes:

    public class MyContract : IContract {
        public bool CanProcess(Type type) {
            return typeof(MyClass) == type;
        }

        public void Write(Serializer serializer, object value) {
            var myClass = (MyClass)value;
            serializer.Serialize(myClass.myField);
            serializer.Serialize(myClass.MyProperty);
        }

        public object Read(Deserializer deserializer, Type type = null, object existingValue = null) {
            var myClass = (MyClass)existingValue;
            myClass.myField = deserializer.Deserialize<int>();
            myClass.MyProperty = deserializer.Deserialize<string>();
            return myClass;
        }
    }

	// Construct a Contractor instance that uses the custom contract:
	var customContractor = new Contractor(new MyContract());
	// This Serializer will use the custom contract when a class that
	// the contract can process is serialized.
	var serializer = new Serializer(outputStream, customContractor);

Data serialized with a customised `Contractor` instance **must** be
deserialized using a `Contractor` instance that uses the same custom contracts
and contract factories.

#### ISelfContract ####

As an alternative to defining an `IContract` implementation, a class or struct
can instead implement the `ISelfContract` interface to control their own
serialization:

	public class SelfContracted : ISelfContract {

		public int aField;

		private bool ABoolean {
			get;
			set;
		}
		
		void ISelfContract.Write(Serializer serializer) {
			serializer.Serialize(aNumber);
			serializer.Serialize(aBoolean);
		}
		
		void ISelfContract.Read(Deserializer deserializer) {
			aNumber = deserializer.ReadInt32();
			aBoolean = deserializer.ReadBoolean();
		}
	}

---

## Thoughts? ##

Email: SurlyWyvern\[at\]gmail.com for questions or suggestions.
