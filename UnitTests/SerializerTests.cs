﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SurlyWyvern.Herald;
namespace UnitTests {
	[TestClass]
	public class SerializerTests {
		#region Constructor Tests

		[TestMethod]
		[Description("Ensure null parameters for constructors throw exceptions.")]
		public void SerializerCtorNullParamsTest() {
			Serializer serializer = null;
			MemoryStream dummyStream = new MemoryStream(8);
			
			// Null Stream
			try {
				serializer = new Serializer((Stream)null);
				Assert.Fail("Null Stream parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("stream", xcpt.ParamName, "Null Stream parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null Stream parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}

			try {
				serializer = new Serializer((Stream)null, new SerializerSession());
				Assert.Fail("Null Stream parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("stream", xcpt.ParamName, "Null Stream parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null Stream parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}

			// Null filepath
			try {
				serializer = new Serializer((string)null);
				Assert.Fail("Null filepath parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("filepath", xcpt.ParamName, "Null filepath parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null filepath parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}

			try {
				serializer = new Serializer((string)null, new SerializerSession());
				Assert.Fail("Null filepath parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("filepath", xcpt.ParamName, "Null filepath parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null filepath parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}
			
			try {
				serializer = new Serializer(dummyStream, (SerializerSession)null);
				Assert.Fail("Null SerializerSession parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("session", xcpt.ParamName, "Null SerializerSession parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null SerializerSession parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}

			try {
				serializer = new Serializer("dummy_filepath", (SerializerSession)null);
				Assert.Fail("Null SerializerSession parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("session", xcpt.ParamName, "Null SerializerSession parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null SerializerSession parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}
		}

		[TestMethod]
		[Description("Ensure un-writeable Stream parameters for constructors throw exceptions.")]
		public void SerializerCtorWriteableStreamTest() {
			Serializer serializer = null;
			MemoryStream dummyStream = new MemoryStream(new byte[8], false);
			
			try {
				serializer = new Serializer(dummyStream);
				Assert.Fail("Non-writeable Stream parameter failed to throw exception.");
			} catch(ArgumentException xcpt) {
				Assert.AreEqual("stream", xcpt.ParamName, "Non-writeable stream parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Non-writeable stream parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}

			try {
				serializer = new Serializer(dummyStream, new SerializerSession());
				Assert.Fail("Non-writeable Stream parameter failed to throw exception.");
			} catch(ArgumentException xcpt) {
				Assert.AreEqual("stream", xcpt.ParamName, "Non-writeable stream parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Non-writeable stream parameter threw incorrect exception: " + xcpt);
			} finally {
				serializer?.Dispose();
			}

		}

		#endregion

		#region Serialization Tests

		[TestMethod]
		[Description("Ensures that all primitive data types are serialized correctly.")]
		public void SerializerPrimitiveTest() {
			using(var stream = new MemoryStream(16))
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream)) {
				// Bool
				serializer.Serialize(true);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.IsTrue(reader.ReadBoolean(), "Serialize(bool) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Byte
				serializer.Serialize((byte)7);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual((byte)7, (byte)stream.ReadByte(), "Serialize(byte) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Int16
				serializer.Serialize((Int16)1001);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual((Int16)1001, reader.ReadInt16(), "Serialize(Int16) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Int32
				serializer.Serialize(42024);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(42024, reader.ReadInt32(), "Serialize(Int32) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Int64
				serializer.Serialize(87650005678L);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(87650005678L, reader.ReadInt64(), "Serialize(Int64) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Single
				serializer.Serialize(-68.93f);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(-68.93f, reader.ReadSingle(), "Serialize(Float32) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Double
				serializer.Serialize(34535.123);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(34535.123, reader.ReadDouble(), "Serialize(Float64) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// SByte
				serializer.Serialize((SByte)(-45));
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual((SByte)(-45), reader.ReadSByte(), "Serialize(SByte) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Char
				serializer.Serialize('h');
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual('h', reader.ReadChar(), "Serialize(Char) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// UInt16
				serializer.Serialize((UInt16)1001);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual((UInt16)1001, reader.ReadUInt16(), "Serialize(UInt16) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// UInt32
				serializer.Serialize((UInt32)42024);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual((UInt32)42024, reader.ReadUInt32(), "Serialize(UInt32) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// UInt64
				serializer.Serialize((UInt64)87650005678);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual((UInt64)87650005678, reader.ReadUInt64(), "Serialize(UInt64) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

			}
		}

		[TestMethod]
		[Description("Ensures that non-primitive built-in value types are serialized correctly.")]
		public void SerializerBuiltinStructsTest() {
			using(var stream = new MemoryStream(16))
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream)) {
				// Decimal
				serializer.SerializeExplicit(123.45m);
				stream.Seek(0, SeekOrigin.Begin);
				var token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.Decimal, "Serialize(Decimal) failure; incorrect Token.");
				int lo = reader.ReadInt32();
				int mid = reader.ReadInt32();
				int hi = reader.ReadInt32();
				bool negative = reader.ReadBoolean();
				byte scale = reader.ReadByte();
				Assert.AreEqual(123.45m, new decimal(lo, mid, hi, negative, scale), "Serialize(Decimal) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// DateTime
				DateTime dt = new DateTime(1234, 5, 6);
				serializer.SerializeExplicit(dt);
				stream.Seek(0, SeekOrigin.Begin);
				token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.DateTime, "Serialize(DateTime) failure; incorrect Token.");
				Assert.AreEqual(dt, new DateTime(reader.ReadInt64()), "Serialize(DateTime) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// TimeSpan
				TimeSpan ts = new TimeSpan(1, 30, 0);
				serializer.SerializeExplicit(ts);
				stream.Seek(0, SeekOrigin.Begin);
				token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.TimeSpan, "Serialize(TimeSpan) failure; incorrect Token.");
				Assert.AreEqual(ts, new TimeSpan(reader.ReadInt64()), "Serialize(TimeSpan) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);


				// DateTimeOffset
				DateTimeOffset dto = new DateTimeOffset(dt, ts);
				serializer.SerializeExplicit(dto);
				stream.Seek(0, SeekOrigin.Begin);
				token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.DateTimeOffset, "Serialize(DateTimeOffset) failure; incorrect Token.");
				Assert.AreEqual(dto, new DateTimeOffset(reader.ReadInt64(), TimeSpan.FromMinutes(reader.ReadInt16())), "Serialize(DateTimeOffset) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Guid
				Guid guid = Guid.NewGuid();
				serializer.SerializeExplicit(guid);
				stream.Seek(0, SeekOrigin.Begin);
				token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.Guid, "Serialize(Guid) failure; incorrect Token.");
				Assert.AreEqual(guid, new Guid(reader.ReadBytes(16)), "Serialize(Guid) failure; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);
			}
		}

		[TestMethod]
		[Description("Ensures that strings are serialized correctly.")]
		public void SerializerStringsTest() {
			using(var stream = new MemoryStream())
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream, new SerializerSession(false))) {
				var string1 = "Hello, ";
				var string2 = "World!";
				serializer.SerializeExplicit(string1);
				serializer.SerializeExplicit(string2);
				serializer.SerializeExplicit(string1);

				stream.Seek(0, SeekOrigin.Begin);

				var token = reader.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.String, "Serialize(String) without referencing failed; incorrect token.");
				Assert.AreEqual(string1, reader.ReadString(), "Serialize(String) without referencing failed; incorrect value.");

				token = reader.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.String, "Serialize(String) without referencing failed; incorrect token.");
				Assert.AreEqual(string2, reader.ReadString(), "Serialize(String) without referencing failed; incorrect value.");

				token = reader.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.String, "Serialize(String) without referencing failed; incorrect token.");
				Assert.AreEqual(string1, reader.ReadString(), "Serialize(String) without referencing failed; incorrect value.");
			}

			// Referencing
			using(var stream = new MemoryStream())
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream)) {
				var string1 = "Cyborg. ";
				var string2 = "Dinosaur.";
				serializer.SerializeExplicit(string1);
				serializer.SerializeExplicit(string2);
				serializer.SerializeExplicit(string1);
				var endPos = stream.Position;

				stream.Seek(0, SeekOrigin.Begin);

				var token = reader.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.StringRef, "Serialize(String) failed; incorrect token.");
				Assert.AreEqual(1, reader.ReadInt32(), "Serialize(String) failed, incorrect reference value.");
				Assert.AreEqual(string1, reader.ReadString(), "Serialize(String) failed; incorrect value.");

				token = reader.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.StringRef, "Serialize(String) failed; incorrect token.");
				Assert.AreEqual(2, reader.ReadInt32(), "Serialize(String) failed, incorrect reference value.");
				Assert.AreEqual(string2, reader.ReadString(), "Serialize(String) failed; incorrect value.");

				token = reader.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.Ref, "Serialize(String) referenced string failed; incorrect token.");
				Assert.AreEqual(1, reader.ReadInt32(), "Serialize(String) referenced string failed; incorect reference.");

				Assert.AreEqual(endPos, stream.Position, "Serialize(String) failed; did not read and write the same lengths.");
			}

			// Extreme strings
			using(var stream = new MemoryStream())
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream)) {
				var string1 =
@"This string is over 256 characters long; the length of the default Serializer buffer. This tests whether the Serializer will correctly write such large strings. It needs to perform multiple writes from the buffer.
GibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberish
GibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishFishGibberishGibberishGibberishGibberishGibberishGibberish";
				serializer.SerializeExplicit(string1);
				var endPos = stream.Position;

				stream.Seek(0, SeekOrigin.Begin);

				var token = reader.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.StringRef, "Serialize(String) failed; incorrect token.");
				Assert.AreEqual(1, reader.ReadInt32(), "Serialize(String) failed, incorrect reference value.");
				Assert.AreEqual(string1, reader.ReadString(), "Serialize(String) failed; incorrect value.");

				Assert.AreEqual(endPos, stream.Position, "Serialize(String) failed; did not read and write the same lengths.");
			}

		}

		[TestMethod]
		[Description("Ensures that Types are serialized correctly.")]
		public void SerializerTypesTest() {
			using(var stream = new MemoryStream())
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream, new SerializerSession(false))) {
				var type1 = typeof(Serializer);
				serializer.SerializeExplicit(type1);
				stream.Seek(0, SeekOrigin.Begin);

				var token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.Type, "Serialize(Type) failed; incorrect Token.");
				Assert.AreEqual(type1, serializer.Binder.BindToType(reader.ReadString()), "Serialize(Type) failed; incorrect deserialized type.");
			}

			using(var stream = new MemoryStream())
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream)) {
				var type1 = typeof(Serializer);
				var type2 = typeof(int);
				serializer.SerializeExplicit(type1);
				serializer.SerializeExplicit(type2);
				serializer.SerializeExplicit(type1);
				stream.Seek(0, SeekOrigin.Begin);

				var token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.TypeRef, "Serialize(TypeRef) failed; incorrect Token.");
				Assert.AreEqual(1, reader.ReadInt32(), "Serialize(TypeRef) failed; incorrect reference id.");
				Assert.AreEqual(type1, serializer.Binder.BindToType(reader.ReadString()), "Serialize(TypeRef) failed; incorrect deserialized type.");

				token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.TypeRef, "Serialize(TypeRef) failed; incorrect Token.");
				Assert.AreEqual(2, reader.ReadInt32(), "Serialize(TypeRef) failed; incorrect reference id.");
				Assert.AreEqual(type2, serializer.Binder.BindToType(reader.ReadString()), "Serialize(TypeRef) failed; incorrect deserialized type.");

				token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.Ref, "Serialize(TypeRef) failed; incorrect Token.");
				Assert.AreEqual(1, reader.ReadInt32(), "Serialize(TypeRef) failed; incorrect reference id.");
			}


		}

		[TestMethod]
		[Description("Ensures that Enums are serialized correctly.")]
		public void SerializerEnumsTest() {
			using(var stream = new MemoryStream())
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream, new SerializerSession(false))) {
				var value = Color.Green;

				serializer.SerializeExplicit(value);
				stream.Seek(0, SeekOrigin.Begin);

				var token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.Enum, "Serialize(Enum) failed; incorrect Token.");
				token = stream.ReadByte();
				Assert.IsTrue(token >= 0 && (Token)token == Token.Type, "Serialize(Enum) failed; incorrect Token for type.");
				Assert.AreEqual(typeof(Color), serializer.Binder.BindToType(reader.ReadString()), "Serialize(Enum) failed; incorrect type deserialized.");
				Assert.IsTrue((Color)reader.ReadInt32() == value, "Serialize(Enum) failed; incorrect value.");
			}
		}

		[TestMethod]
		[Description("Ensures that null values are serialized correctly.")]
		public void SerializerNullTest() {
			using(var stream = new MemoryStream(new byte[1]))
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream)) {
				object nullObject = null;
				serializer.SerializeExplicit(nullObject);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(Token.Null, (Token)reader.ReadByte(), "Null reference did not serialize correctly.");
			}
		}
		
		[TestMethod]
		[Description("Ensures that nullables are serialized correctly.")]
		public void SerializerNullableTest() {
			using(var stream = new MemoryStream())
			using(var reader = new BinaryReader(stream))
			using(var serializer = new Serializer(stream)) {
				int? x = null;
				serializer.SerializeExplicit(x);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(Token.Null, (Token)reader.ReadByte(), "Serialize(int?) failed; Incorrect token.");
				stream.Seek(0, SeekOrigin.Begin);

				x = 3;
				serializer.SerializeExplicit(x);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(Token.Int32, (Token)reader.ReadByte(), "Serialize(int?) failed; Incorrect token.");
				Assert.AreEqual(x, reader.ReadInt32(), "Serialize(int?) failed; Incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Serializer implements an overload of Serialize, purely for
				// nullable types.
				x = null;
				serializer.Serialize(x);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(Token.Null, (Token)reader.ReadByte(), "SerializeImplicit(int?) failed; Incorrect token.");
				stream.Seek(0, SeekOrigin.Begin);

				x = 72;
				serializer.Serialize(x);
				stream.Seek(0, SeekOrigin.Begin);
				Assert.AreEqual(Token.Int32, (Token)reader.ReadByte(), "SerializeImplicit(int?) failed; Incorrect token.");
				Assert.AreEqual(72, reader.ReadInt32(), "SerializeImplicit(int?) failed; Incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);
			}
		}

		[TestMethod]
		[Description("Ensures that self-referencing loops are detected.")]
		public void SerializerSelfReferencingTest() {
			var loop = new SelfReferenceClass();
			var loop2 = new SelfReferenceClass();
			loop.other = loop2;
			loop2.other = loop;

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream, new SerializerSession(false, ReferenceLoopHandling.Exception))) {
				try {
					serializer.SerializeExplicit(loop);
					Assert.Fail("Self-referencing loop did not cause exception.");
				} catch(SerializationException xcpt) {
					Assert.IsTrue(xcpt.Message.Contains("Self-referencing loop"), "Self-referencing loop test threw incorrect SerializationException.");
				} catch(Exception xcpt) {
					Assert.Fail("Self-referencing loop threw incorrect Exception: " + xcpt);
				}
			}

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream, new SerializerSession(false, ReferenceLoopHandling.Nullify)))
			using(var deserializer = new Deserializer(stream)) {
				try {
					serializer.SerializeExplicit(loop);
				} catch(Exception) {
					Assert.Fail("Nullified self-referencing loop threw exception.");
				}

				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize() as SelfReferenceClass;
				Assert.IsNotNull(result, "Nullified self-referencing loop did not deserialize.");
				Assert.IsNotNull(result.other, "Nullified self-referencing loop nullified incorrectly.");
				Assert.IsNull(result.other.other, "Nullified self-referencing loop did not nullify correctly.");
			}
		}

		#endregion
	}
}
