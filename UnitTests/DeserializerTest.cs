﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using SurlyWyvern.Herald;

namespace UnitTests {
	[TestClass]
	public class DeserializerTest {
		#region Constructor Tests

		[TestMethod]
		[Description("Ensures that null arguements throw exceptions.")]
		public void DeserializerCtorNullArgsTest() {
			Deserializer deserializer = null;
			MemoryStream dummyStream = new MemoryStream();
			// Null Stream
			try {
				deserializer = new Deserializer((Stream)null);
				Assert.Fail("Null Stream parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("stream", xcpt.ParamName, "Null Stream parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null Stream parameter threw incorrect exception: " + xcpt);
			} finally {
				deserializer?.Dispose();
			}

			// Null Filepath
			try {
				deserializer = new Deserializer((string)null);
				Assert.Fail("Null filepath parameter failed to throw exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("filepath", xcpt.ParamName, "Null filepath parameter threw incorrect ArgumentNullException.");
			} catch(Exception xcpt) {
				Assert.Fail("Null filepath parameter threw incorrect exception: " + xcpt);
			} finally {
				deserializer?.Dispose();
			}

		}

		#endregion

		#region Deserialization Tests

		[TestMethod]
		[Description("Ensures that primitive types are correctly deserialized.")]
		public void DeserializerPrimitivesTest() {
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				// Boolean
				serializer.Serialize((bool?)true);
				stream.Seek(0, SeekOrigin.Begin);
				object result = deserializer.Deserialize(typeof(bool));
				Assert.IsNotNull(result, "Deserialize(bool) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(bool), "Deserialize(bool) failed; result was incorrect type.");
				Assert.IsTrue((bool)result, "Deserialize(bool) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Byte
				serializer.Serialize((byte)44);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(byte));
				Assert.IsNotNull(result, "Deserialize(byte) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(byte), "Deserialize(byte) failed; result was incorrect type.");
				Assert.AreEqual((byte)44, (byte)result, "Deserialize(byte) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Int16
				serializer.Serialize((short)77);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(short));
				Assert.IsNotNull(result, "Deserialize(Int16) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Int16), "Deserialize(Int16) failed; result was incorrect type.");
				Assert.AreEqual((Int16)77, (Int16)result, "Deserialize(Int16) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Int32
				serializer.Serialize(55);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(Int32));
				Assert.IsNotNull(result, "Deserialize(Int32) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Int32), "Deserialize(Int32) failed; result was incorrect type.");
				Assert.AreEqual(55, (int)result, "Deserialize(Int32) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Int64
				serializer.Serialize(1028832L);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(Int64));
				Assert.IsNotNull(result, "Deserialize(Int64) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Int64), "Deserialize(Int64) failed; result was incorrect type.");
				Assert.AreEqual(1028832L, (Int64)result, "Deserialize(Int64) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Float
				serializer.Serialize(123.45f);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(float));
				Assert.IsNotNull(result, "Deserialize(float) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(float), "Deserialize(float) failed; result was incorrect type.");
				Assert.AreEqual(123.45f, (float)result, "Deserialize(float) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Double
				serializer.Serialize(67.89);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(double));
				Assert.IsNotNull(result, "Deserialize(double) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(double), "Deserialize(double) failed; result was incorrect type.");
				Assert.AreEqual(67.89, (double)result, "Deserialize(double) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);
				
				// SByte
				serializer.Serialize((SByte)(-3));
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(SByte));
				Assert.IsNotNull(result, "Deserialize(SByte) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(SByte), "Deserialize(SByte) failed; result was incorrect type.");
				Assert.AreEqual((SByte)(-3), (SByte)result, "Deserialize(SByte) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Char
				serializer.Serialize('h');
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(Char));
				Assert.IsNotNull(result, "Deserialize(Char) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Char), "Deserialize(Char) failed; result was incorrect type.");
				Assert.AreEqual('h', (char)result, "Deserialize(Char) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// UInt16
				serializer.Serialize((ushort)77);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(UInt16));
				Assert.IsNotNull(result, "Deserialize(UInt16) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(UInt16), "Deserialize(UInt16) failed; result was incorrect type.");
				Assert.AreEqual((UInt16)77, (UInt16)result, "Deserialize(UInt16) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// UInt32
				serializer.Serialize((uint)55);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(UInt32));
				Assert.IsNotNull(result, "Deserialize(UInt32) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(UInt32), "Deserialize(UInt32) failed; result was incorrect type.");
				Assert.AreEqual((uint)55, (uint)result, "Deserialize(UInt32) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// UInt64
				serializer.Serialize((ulong)4234);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(UInt64));
				Assert.IsNotNull(result, "Deserialize(UInt64) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(UInt64), "Deserialize(UInt64) failed; result was incorrect type.");
				Assert.AreEqual((ulong)4234, (ulong)result, "Deserialize(UInt64) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

			}
		}

		[TestMethod]
		[Description("Ensures that builtin value types are correctly deserialized.")]
		public void DeserializerBuiltinStructsTest() {
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				// Decimal
				var dec = 132.54m;
				serializer.Serialize(dec);
				stream.Seek(0, SeekOrigin.Begin);
				var result = deserializer.Deserialize(typeof(Decimal));
				Assert.IsNotNull(result, "Deserialize(Decimal) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Decimal), "Deserialize(Decimal) failed; result was incorrect type.");
				Assert.AreEqual(dec, (Decimal)result, "Deserialize(Decimal) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// DateTime
				var dt = DateTime.Now;
				serializer.Serialize(dt);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(DateTime));
				Assert.IsNotNull(result, "Deserialize(DateTime) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(DateTime), "Deserialize(DateTime) failed; result was incorrect type.");
				Assert.AreEqual(dt, (DateTime)result, "Deserialize(DateTime) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// TimeSpan
				var ts = new TimeSpan(1, 20, 3);
				serializer.Serialize(ts);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(TimeSpan));
				Assert.IsNotNull(result, "Deserialize(TimeSpan) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(TimeSpan), "Deserialize(TimeSpan) failed; result was incorrect type.");
				Assert.AreEqual(ts, (TimeSpan)result, "Deserialize(TimeSpan) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// DateTimeOffset
				var dto = new DateTimeOffset(DateTime.SpecifyKind(dt, DateTimeKind.Unspecified), new TimeSpan(1, 30, 0));
				serializer.Serialize(dto);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(DateTimeOffset));
				Assert.IsNotNull(result, "Deserialize(DateTimeOffset) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(DateTimeOffset), "Deserialize(DateTimeOffset) failed; result was incorrect type.");
				Assert.AreEqual(dto, (DateTimeOffset)result, "Deserialize(DateTimeOffset) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				// Guid
				var id = Guid.NewGuid();
				serializer.Serialize(id);
				stream.Seek(0, SeekOrigin.Begin);
				result = deserializer.Deserialize(typeof(Guid));
				Assert.IsNotNull(result, "Deserialize(Guid) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Guid), "Deserialize(Guid) failed; result was incorrect type.");
				Assert.AreEqual(id, (Guid)result, "Deserialize(Guid) failed; result had incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);
			}
		}

		[TestMethod]
		[Description("Ensures that strings are correctly deserialized.")]
		public void DeserializerStringTest() {
			var string1 = "Volcano";
			var string2 =
@"This string is exactly 253 characters. Together with its token and length prefix, it will fill the default buffer to check that the Write(string) method does not accidentally overflow the buffer. GibberishGibberishGibberishGibberishGibberishGibberish";
			var string3 =
@"This string is over 256 characters long; the length of the default Deserializer buffer. This tests whether the Deserializer will correctly read such large strings. It needs to perform multiple reads to the buffer.
GibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberish
GibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishGibberishFishGibberishGibberishGibberishGibberishGibberishGibberish";
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream, new SerializerSession(false)))
			using(var deserializer = new Deserializer(stream)) {
				serializer.Serialize(string1);
				serializer.Serialize(string.Empty);
				serializer.Serialize(string2);
				serializer.Serialize(string3);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize(typeof(string));
				Assert.IsNotNull(result, "Deserialize(String) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(string), "Deserialize(String) failed; result was incorrect type.");
				Assert.AreEqual(string1, (string)result, "Deserialize(String) failed; result had incorrect value.");
				
				result = deserializer.Deserialize(typeof(string));
				Assert.IsNotNull(result, "Deserialize(String) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(string), "Deserialize(String) failed; result was incorrect type.");
				Assert.AreEqual(string.Empty, (string)result, "Deserialize(String) failed; result had incorrect value.");

				result = deserializer.Deserialize(typeof(string));
				Assert.IsNotNull(result, "Deserialize(String) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(string), "Deserialize(String) failed; result was incorrect type.");
				Assert.AreEqual(string2, (string)result, "Deserialize(String) failed; result had incorrect value.");

				result = deserializer.Deserialize(typeof(string));
				Assert.IsNotNull(result, "Deserialize(String) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(string), "Deserialize(String) failed; result was incorrect type.");
				Assert.AreEqual(string3, (string)result, "Deserialize(String) failed; result had incorrect value.");
			}

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				serializer.Serialize(string1);
				serializer.Serialize(string1);
				stream.Seek(0, SeekOrigin.Begin);
					
				var result = deserializer.Deserialize(typeof(string));
				Assert.IsNotNull(result, "Deserialize(StringRef) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(string), "Deserialize(StringRef) failed; result was incorrect type.");
				Assert.AreEqual(string1, (string)result, "Deserialize(StringRef) failed; result had incorrect value.");
				
				result = deserializer.Deserialize(typeof(string));
				Assert.IsNotNull(result, "Deserialize(StringRef) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(string), "Deserialize(StringRef) failed; result was incorrect type.");
				Assert.AreEqual(string1, (string)result, "Deserialize(StringRef) failed; result had incorrect value.");
			}
		}

		[TestMethod]
		[Description("Ensures that types are correctly deserialized.")]
		public void DeserializerTypeTest() {
			var type1 = typeof(int);
			var type2 = typeof(Serializer);

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream, new SerializerSession(false)))
			using(var deserializer = new Deserializer(stream)) {
				serializer.Serialize(type1);
				serializer.Serialize(type2);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize(typeof(Type));
				Assert.IsNotNull(result, "Deserialize(Type) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Type), "Deserialize(Type) failed; result was incorrect type.");
				Assert.AreEqual(type1, (Type)result, "Deserialize(Type) failed; result had incorrect value.");
				result = deserializer.Deserialize(typeof(Type));
				Assert.IsNotNull(result, "Deserialize(Type) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Type), "Deserialize(Type) failed; result was incorrect type.");
				Assert.AreEqual(type2, (Type)result, "Deserialize(Type) failed; result had incorrect value.");
			}

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				serializer.Serialize(type1);
				serializer.Serialize(type2);
				stream.Seek(0, SeekOrigin.Begin);
				
				var result = deserializer.Deserialize(typeof(Type));
				Assert.IsNotNull(result, "Deserialize(Type) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Type), "Deserialize(Type) failed; result was incorrect type.");
				Assert.AreEqual(type1, (Type)result, "Deserialize(Type) failed; result had incorrect value.");
				result = deserializer.Deserialize(typeof(Type));
				Assert.IsNotNull(result, "Deserialize(Type) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Type), "Deserialize(Type) failed; result was incorrect type.");
				Assert.AreEqual(type2, (Type)result, "Deserialize(Type) failed; result had incorrect value.");
			}
		}

		[TestMethod]
		[Description("Ensures that enums are correctly deserialized.")]
		public void DeserializerEnumsTest() {
			var color = Color.Green;

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				serializer.Serialize(color);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize(typeof(Color));
				Assert.IsNotNull(result, "Deserialize(Enum) failed; result was null.");
				Assert.IsInstanceOfType(result, typeof(Color), "Deserialize(Enum) failed; result was incorrect type.");
				Assert.AreEqual(color, (Color)result, "Deserialize(Enum) failed; result had incorrect value.");
			}
		}
		
		[TestMethod]
		[Description("Ensures that nullabe types are correctly deserialized.")]
		public void DeserializerNullablesTest() {
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				int? x = null;
				serializer.Serialize(x);
				stream.Seek(0, SeekOrigin.Begin);
				x = (int?)deserializer.Deserialize(typeof(int?));
				Assert.AreEqual(null, x, "Deserialize(int?) failed; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				x = 33;
				serializer.Serialize(x);
				stream.Seek(0, SeekOrigin.Begin);
				x = (int?)deserializer.Deserialize(typeof(int?));
				Assert.AreEqual(33, x, "Deserialize(int?) failed; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);


				ContractedStruct? y = null;
				serializer.Serialize(y);
				stream.Seek(0, SeekOrigin.Begin);
				var y2 = (ContractedStruct?)deserializer.Deserialize(typeof(ContractedStruct?));
				Assert.AreEqual(null, y, "Deserialize(struct?) failed; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

				y = new ContractedStruct(10, -5);
				serializer.Serialize(y);
				stream.Seek(0, SeekOrigin.Begin);
				y2 = (ContractedStruct?)deserializer.Deserialize(typeof(ContractedStruct?));
				Assert.AreEqual(y, y2, "Deserialize(struct?) failed; incorrect value.");
				stream.Seek(0, SeekOrigin.Begin);

			}
		}

		[TestMethod]
		[Description("Ensures that ISelfContract implementations are used.")]
		public void SelfContractTest() {
			// Self-contracted class
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var test1 = new SelfContractClass(-24, 12);

				serializer.Serialize(test1);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize<SelfContractClass>();
				Assert.IsNotNull(result, "Failed to deserialize a SelfContractClass instance; result is null.");
				Assert.IsInstanceOfType(result, typeof(SelfContractClass), "Failed to deserialize a SelfContractClass instance; result is incorrect type.");
				// The Self Contract only serializes the Length property.
				Assert.IsTrue(result.Length == test1.Length && result.offset == default(ulong), "Failed to deserialize a SelfContractClass instance; result is incorrect value.");

			}

			// Self-contracted struct
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var test = new SelfContractStruct(2, 4);

				serializer.Serialize(test);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize<SelfContractStruct>();
				Assert.IsNotNull(result, "Failed to deserialize a SelfContractStruct instance; result is null.");
				Assert.IsInstanceOfType(result, typeof(SelfContractStruct), "Failed to deserialize a SelfContractStruct instance; result is incorrect type.");
				// The Self Contract only serializes the 'second' field.
				Assert.IsTrue(result.second == test.second && result.First == default(byte), "Failed to deserialize a SelfContractStruct instance; result is incorrect value.");

			}
		}

		[TestMethod]
		[Description("Ensures that arrays are serialized and deserialized correctly.")]
		public void ArrayTest() {
			// int[]
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new int[] { 3, 2, 1, -1, -2, -3 };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "int[] did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(int[]));
				var arr2 = (int[])result;
				Assert.AreEqual(arr.Length, arr2.Length, "int[] did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Length; ++i)
					Assert.AreEqual(arr[i], arr2[i], "int[] did not deserialize correctly; result has a differing element.");
			}

			// Guid[]
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new Guid[] { Guid.NewGuid(), Guid.NewGuid() };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "Guid[] did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(Guid[]));
				var arr2 = (Guid[])result;
				Assert.AreEqual(arr.Length, arr2.Length, "Guid[] did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Length; ++i)
					Assert.AreEqual(arr[i], arr2[i], "Guid[] did not deserialize correctly; result has a differing element.");
			}

			// String[]
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var x = "Profound";
				var arr = new string[] { null, x, "A string", x };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "string[] did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(string[]));
				var arr2 = (string[])result;
				Assert.AreEqual(arr.Length, arr2.Length, "string[] did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Length; ++i)
					Assert.AreEqual(arr[i], arr2[i], "string[] did not deserialize correctly; result has a differing element.");
			}

			// Type[]
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var x = typeof(Serializer);
				var arr = new Type[] { null, x, typeof(bool), x };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "Type[] did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(Type[]));
				var arr2 = (Type[])result;
				Assert.AreEqual(arr.Length, arr2.Length, "Type[] did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Length; ++i)
					Assert.AreEqual(arr[i], arr2[i], "Type[] did not deserialize correctly; result has a differing element.");
			}

			// byte?[]
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new byte?[] { null, 0, 33, null, 90 };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "byte?[] did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(byte?[]));
				var arr2 = (byte?[])result;
				Assert.AreEqual(arr.Length, arr2.Length, "byte?[] did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Length; ++i)
					Assert.AreEqual(arr[i], arr2[i], "byte?[] did not deserialize correctly; result has a differing element.");
			}

			// Array of generated contract class.
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var x = new ContractedClass(1, 2.3f);
				var arr = new ContractedClass[] { x, null, x, new ContractedClass(4, 5.6f) };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize() as ContractedClass[];
				Assert.IsNotNull(result, "ILContract array did not deserialize correctly; result is null.");
				Assert.AreEqual(arr.Length, result.Length, "ILContract array did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Length; ++i) {
					if(arr[i] is null) {
						Assert.IsNull(result[i]);
					} else {
						Assert.IsNotNull(result[i]);
						Assert.IsTrue(arr[i].Equals(result[i]));
					}
				}
			}

			// Array of Arrays
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new int[][] { new int[] { 1, 2 }, null, new int[] { -3, -4 } };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "int[][] did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(int[][]));
				var arr2 = (int[][])result;
				Assert.AreEqual(arr.Length, arr2.Length, "int[][] did not deserialize correctly; result is incorrect length");
				for(int i = 0; i < arr.Length; ++i) {
					if(arr[i] is null) {
						Assert.IsNull(arr2[i], "int[][] did not deserialize correctly; result element is not null.");
					} else {
						Assert.IsNotNull(arr2[i], "int[][] did not deserialize correctly; result element is null.");
						Assert.AreEqual(arr[i].Length, arr2[i].Length, "int[][] did not deserialize correctly; result element has incorrect length.");
						for(int j = 0; j < arr[i].Length; ++j)
							Assert.AreEqual(arr[i][j], arr2[i][j], "int[][] did not deserialize correctly; result element element has incorrect value.");
					}
				}
			}
		}

		[TestMethod]
		[Description("Ensures that generic lists are serialized and deserialized correctly.")]
		public void ListTest() {
			// int
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new List<int>() { 3, 2, 1, -1, -2, -3 };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "List<int> did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(List<int>));
				var arr2 = (List<int>)result;
				Assert.AreEqual(arr.Count, arr2.Count, "List<int> did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Count; ++i)
					Assert.AreEqual(arr[i], arr2[i], "List<int> did not deserialize correctly; result has a differing element.");
			}

			// Guid
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new List<Guid>() { Guid.NewGuid(), Guid.NewGuid() };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "List<Guid> did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(List<Guid>));
				var arr2 = (List<Guid>)result;
				Assert.AreEqual(arr.Count, arr2.Count, "List<Guid> did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Count; ++i)
					Assert.AreEqual(arr[i], arr2[i], "List<Guid> did not deserialize correctly; result has a differing element.");
			}

			// String
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var x = "Profound";
				var arr = new List<string>() { null, x, "A string", x };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "List<string> did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(List<string>));
				var arr2 = (List<string>)result;
				Assert.AreEqual(arr.Count, arr2.Count, "List<string> did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Count; ++i)
					Assert.AreEqual(arr[i], arr2[i], "List<string> did not deserialize correctly; result has a differing element.");
			}

			// Type
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var x = typeof(Serializer);
				var arr = new List<Type>() { null, x, typeof(bool), x };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "List<Type> did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(List<Type>));
				var arr2 = (List<Type>)result;
				Assert.AreEqual(arr.Count, arr2.Count, "List<Type> did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Count; ++i)
					Assert.AreEqual(arr[i], arr2[i], "List<Type> did not deserialize correctly; result has a differing element.");
			}

			// byte?
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new List<byte?> { null, 0, 33, null, 90 };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "List<byte?> did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(List<byte?>));
				var arr2 = (List<byte?>)result;
				Assert.AreEqual(arr.Count, arr2.Count, "List<byte?> did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Count; ++i)
					Assert.AreEqual(arr[i], arr2[i], "List<byte?> did not deserialize correctly; result has a differing element.");
			}

			// Generated contract
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var x = new ContractedClass(1, 2.3f);
				var arr = new List<ContractedClass>() { x, null, x, new ContractedClass(4, 5.6f) };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize() as List<ContractedClass>;
				Assert.IsNotNull(result, "List<ContractedClass> did not deserialize correctly; result is null.");
				Assert.AreEqual(arr.Count, result.Count, "List<ContractedClass> did not deserialize correctly; result has differing length.");
				for(int i = 0; i < arr.Count; ++i)
					if(arr[i] is null) {
						Assert.IsNull(result[i]);
					} else {
						Assert.IsNotNull(result[i]);
						Assert.IsTrue(arr[i].Equals(result[i]));
					}
			}

			// List of Lists
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var arr = new List<List<int>>() { new List<int>() { 1, 2 }, null, new List<int>() { -3, -4 } };
				serializer.SerializeExplicit(arr);
				stream.Seek(0, SeekOrigin.Begin);

				var result = deserializer.Deserialize();
				Assert.IsNotNull(result, "List<List<int>> did not deserialize correctly; result is null.");
				Assert.IsInstanceOfType(result, typeof(List<List<int>>));
				var arr2 = (List<List<int>>)result;
				Assert.AreEqual(arr.Count, arr2.Count, "List<List<int>> did not deserialize correctly; result is incorrect length");
				for(int i = 0; i < arr.Count; ++i) {
					if(arr[i] is null) {
						Assert.IsNull(arr2[i], "List<List<int>> did not deserialize correctly; result element is not null.");
						continue;
					} else {
						Assert.IsNotNull(arr2[i], "List<List<int>> did not deserialize correctly; result element is null.");
						Assert.AreEqual(arr[i].Count, arr2[i].Count, "List<List<int>> did not deserialize correctly; result element has incorrect length.");
						for(int j = 0; j < arr[i].Count; ++j)
							Assert.AreEqual(arr[i][j], arr2[i][j], "List<List<int>> did not deserialize correctly; result element element has incorrect value.");
					}
				}
			}
		}

		[TestMethod]
		[Description("Ensures that generic dictionaries are serialized and deserialized correctly.")]
		public void DictionaryContractTest() {

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var dict = new Dictionary<int, int>() {
					[3] = 6,
					[4] = 8,
					[10] = 20,
				};

				serializer.SerializeExplicit(dict);
				stream.Seek(0, SeekOrigin.Begin);
				var result = deserializer.Deserialize() as Dictionary<int, int>;
				Assert.IsNotNull(result);
				Assert.AreEqual(dict.Count, result.Count);
				using(var dictit = dict.GetEnumerator())
				using(var resultit = result.GetEnumerator()) {
					while(dictit.MoveNext()) {
						resultit.MoveNext();
						Assert.AreEqual(dictit.Current, resultit.Current);
					}
				}
			}

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var dict = new Dictionary<int, ContractedClass>() {
					[3] = new ContractedClass(6, 6),
					[4] = new ContractedClass(8, 8),
					[10] = new ContractedClass(20, 20),
				};

				serializer.SerializeExplicit(dict);
				stream.Seek(0, SeekOrigin.Begin);
				var result = deserializer.Deserialize() as Dictionary<int, ContractedClass>;
				Assert.IsNotNull(result);
				Assert.AreEqual(dict.Count, result.Count);
				using(var dictit = dict.GetEnumerator())
				using(var resultit = result.GetEnumerator()) {
					while(dictit.MoveNext()) {
						resultit.MoveNext();
						Assert.AreEqual(dictit.Current.Key, resultit.Current.Key);
						if(dictit.Current.Value is null) {
							Assert.IsNull(resultit.Current.Value);
						} else {
							Assert.IsNotNull(resultit.Current.Value);
							Assert.IsTrue(dictit.Current.Value.Equals(resultit.Current.Value));
						}
					}
				}
			}

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var dict = new Dictionary<ContractedClass, ContractedClass>() {
					[new ContractedClass(3, 3)] = new ContractedClass(6, 6),
					[new ContractedClass(4, 4)] = null,
					[new ContractedClass(10, 10)] = new ContractedClass(20, 20),
				};

				serializer.SerializeExplicit(dict);
				stream.Seek(0, SeekOrigin.Begin);
				var result = deserializer.Deserialize() as Dictionary<ContractedClass, ContractedClass>;
				Assert.IsNotNull(result);
				Assert.AreEqual(dict.Count, result.Count);
				using(var dictit = dict.GetEnumerator())
				using(var resultit = result.GetEnumerator()) {
					while(dictit.MoveNext()) {
						resultit.MoveNext();
						// Keys
						Assert.IsTrue(dictit.Current.Key.Equals(resultit.Current.Key));

						// Values
						if(dictit.Current.Value is null) {
							Assert.IsNull(resultit.Current.Value);
						} else {
							Assert.IsNotNull(resultit.Current.Value);
							Assert.IsTrue(dictit.Current.Value.Equals(resultit.Current.Value));
						}
					}
				}
			}

			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream))
			using(var deserializer = new Deserializer(stream)) {
				var dict = new Dictionary<ContractedClass, int>() {
					[new ContractedClass(3, 3)] = 6,
					[new ContractedClass(4, 4)] = 8,
					[new ContractedClass(10, 10)] = 20,
				};

				serializer.SerializeExplicit(dict);
				stream.Seek(0, SeekOrigin.Begin);
				var result = deserializer.Deserialize() as Dictionary<ContractedClass, int>;
				Assert.IsNotNull(result);
				Assert.AreEqual(dict.Count, result.Count);
				var dictit = dict.GetEnumerator();
				var resultit = result.GetEnumerator();
				while(dictit.MoveNext()) {
					resultit.MoveNext();

					Assert.IsNotNull(resultit.Current.Key);
					Assert.IsTrue(dictit.Current.Key.Equals(resultit.Current.Key));

					Assert.AreEqual(dictit.Current.Value, resultit.Current.Value);
				}
			}

		}

		[TestMethod]
		[Description("Ensures that generic HashSets are serialized and deserialized correctly.")]
		public void HashSetTest() {
			// Object
			using(var stream = new MemoryStream()) {
				var value = new HashSet<object>() { 1, null, Guid.NewGuid(), typeof(Serializer), "Fortify" };
				using(var serializer = new Serializer(stream) { KeepOpen = true }) {
					serializer.Serialize(value);
				}
				stream.Seek(0, SeekOrigin.Begin);
				HashSet<object> deserialized;
				using(var deserializer = new Deserializer(stream)) {
					deserialized = (HashSet<object>)deserializer.Deserialize(typeof(HashSet<object>));
				}
				Assert.IsNotNull(deserialized);
				Assert.AreEqual(value.Count, deserialized.Count);
				foreach(var element in value)
					Assert.IsTrue(deserialized.Contains(element));
			}
			
			// Int
			using(var stream = new MemoryStream()) {
				var value = new HashSet<int>() { 1,	2, 3 };
				using(var serializer = new Serializer(stream) { KeepOpen = true }) {
					serializer.Serialize(value);
				}
				stream.Seek(0, SeekOrigin.Begin);
				HashSet<int> deserialized;
				using(var deserializer = new Deserializer(stream)) {
					deserialized = (HashSet<int>)deserializer.Deserialize(typeof(HashSet<int>));
				}
				Assert.IsNotNull(deserialized);
				Assert.AreEqual(value.Count, deserialized.Count);
				foreach(var element in value)
					Assert.IsTrue(deserialized.Contains(element));
			}

			// double?
			using(var stream = new MemoryStream()) {
				var value = new HashSet<double?>() { 1.0, null, -4.5 };
				using(var serializer = new Serializer(stream) { KeepOpen = true }) {
					serializer.Serialize(value);
				}
				stream.Seek(0, SeekOrigin.Begin);
				HashSet<double?> deserialized;
				using(var deserializer = new Deserializer(stream)) {
					deserialized = (HashSet<double?>)deserializer.Deserialize(typeof(HashSet<double?>));
				}
				Assert.IsNotNull(deserialized);
				Assert.AreEqual(value.Count, deserialized.Count);
				foreach(var element in value)
					Assert.IsTrue(deserialized.Contains(element));
			}

			// String
			using(var stream = new MemoryStream()) {
				var value = new HashSet<string>() { "Hello", null, "", "World!" };
				using(var serializer = new Serializer(stream) { KeepOpen = true }) {
					serializer.Serialize(value);
				}
				stream.Seek(0, SeekOrigin.Begin);
				HashSet<string> deserialized;
				using(var deserializer = new Deserializer(stream)) {
					deserialized = (HashSet<string>)deserializer.Deserialize(typeof(HashSet<string>));
				}
				Assert.IsNotNull(deserialized);
				Assert.AreEqual(value.Count, deserialized.Count);
				foreach(var element in value)
					Assert.IsTrue(deserialized.Contains(element));
			}

			// Struct
			using(var stream = new MemoryStream()) {
				var value = new HashSet<ContractedStruct>() { new ContractedStruct(3, -3), new ContractedStruct(45, 46) };
				using(var serializer = new Serializer(stream) { KeepOpen = true }) {
					serializer.Serialize(value);
				}
				stream.Seek(0, SeekOrigin.Begin);
				HashSet<ContractedStruct> deserialized;
				using(var deserializer = new Deserializer(stream)) {
					deserialized = (HashSet<ContractedStruct>)deserializer.Deserialize(typeof(HashSet<ContractedStruct>));
				}
				Assert.IsNotNull(deserialized);
				Assert.AreEqual(value.Count, deserialized.Count);
				foreach(var element in value)
					Assert.IsTrue(deserialized.Contains(element));
			}
			
		}

		#endregion

		[TestMethod]
		public void DeserializerCreateInstanceTests() {
			// Null type
			try {
				var x = Deserializer.CreateInstance(null);
				Assert.Fail("Deserializer.CreateInstance(null) did not throw an exception.");
			} catch(ArgumentNullException xcpt) {
				Assert.AreEqual("type", xcpt.ParamName);
			} catch(Exception xcpt) {
				Assert.Fail("Deserializer.CreateInstance(null) threw an unexpected exception: " + xcpt);
			}

			// Abstract type
			try {
				var x = Deserializer.CreateInstance(typeof(Stream));
				Assert.Fail("Deserializer.CreateInstance(abstract) did not throw an exception.");
			} catch(ArgumentException xcpt) {
				Assert.IsTrue(xcpt.Message.Contains("abstract"));
			} catch(Exception xcpt) {
				Assert.Fail("Deserializer.CreateInstance(abstract) threw an unexpected exception: " + xcpt);
			}

			// Interface
			try {
				var x = Deserializer.CreateInstance(typeof(IContract));
				Assert.Fail("Deserializer.CreateInstance(interface) did not throw an exception.");
			} catch(ArgumentException xcpt) {
				Assert.IsTrue(xcpt.Message.Contains("interface"));
			} catch(Exception xcpt) {
				Assert.Fail("Deserializer.CreateInstance(interface) threw an unexpected exception: " + xcpt);
			}

			// GenericTypeDefinition
			try {
				var x = Deserializer.CreateInstance(typeof(List<>));
				Assert.Fail("Deserializer.CreateInstance(List<>) did not throw an exception.");
			} catch(ArgumentException xcpt) {
				Assert.IsTrue(xcpt.Message.Contains("generic type def"));
			} catch(Exception xcpt) {
				Assert.Fail("Deserializer.CreateInstance(List<>) threw an unexpected exception: " + xcpt);
			}

		}
		
	}
}
