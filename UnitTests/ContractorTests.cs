﻿using System;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using SurlyWyvern.Herald;
using SurlyWyvern.Herald.DefaultContracts;

namespace UnitTests {
	[TestClass]
	public class ContractorTests {
		[TestMethod]
		[Description("Ensures that changing the member inclusion settings changes the generated contract.")]
		public void ContractorMemberInclusionTest() {

			var contractor = new Contractor(MemberInclusion.PublicOnly);
			var test = new ContractedClass(20, 30.5f);
			using(var stream = new MemoryStream())
			using(var serializer = new Serializer(stream, contractor))
			using(var deserializer = new Deserializer(stream, contractor)) {
				serializer.Serialize(test);
				stream.Seek(0, SeekOrigin.Begin);
				var result = deserializer.Deserialize<ContractedClass>();
				Assert.IsNotNull(result);
				// Private members should not have been serialized, so the instances should not be equal
				Assert.IsFalse(test.Equals(result));
				// Check equality of members that should have been serialized.
				Assert.AreEqual(test.a_Field, result.a_Field);
				Assert.AreEqual(test.A_Property, result.A_Property);
			}
		}

		[TestMethod]
		[Description("Ensures that generated ILContracts correctly serialize all types of members.")]
		public void ContractorILContractTest() {
			using(var stream = new MemoryStream()) {
				var x = new ComplexClass(null, 2.3, Guid.NewGuid(), DateTime.Now, new ContractedStruct(-4, 4), new ContractedStruct(8, -8), new ContractedClass(40, -0.5f), new SubContractedClass(-80, 0.5f, 32), "Frazzle", typeof(Serializer));
				using(var serializer = new Serializer(stream)) {
					// Ensure that an ILContract is generated for this type.
					Assert.IsInstanceOfType(serializer.Contractor.GetContract(typeof(ComplexClass)), typeof(ILContract));
					serializer.Serialize(x);
					serializer.KeepOpen = true;
				}
				stream.Seek(0, SeekOrigin.Begin);
				using(var deserializer = new Deserializer(stream)) {
					var y = (ComplexClass)deserializer.Deserialize(typeof(ComplexClass));
					Assert.IsNotNull(y);
					Assert.IsTrue(x.Equals(y));
				}
			}

			using(var stream = new MemoryStream()) {
				var a = new ComplexStruct(null, 2.3, Guid.NewGuid(), DateTime.Now, new ContractedStruct(-4, 4), new ContractedStruct(8, -8), new ContractedClass(40, -0.5f), new SubContractedClass(-80, 0.5f, 32), "Extremity", typeof(Deserializer));
				using(var serializer = new Serializer(stream)) {
					// Ensure that an ILContract is generated for this type.
					Assert.IsInstanceOfType(serializer.Contractor.GetContract(typeof(ComplexStruct)), typeof(ILContract));
					serializer.Serialize(a);
					serializer.KeepOpen = true;
				}
				stream.Seek(0, SeekOrigin.Begin);
				using(var deserializer = new Deserializer(stream)) {
					var b = (ComplexStruct)deserializer.Deserialize(typeof(ComplexStruct));
					Assert.IsTrue(a.Equals(b));
				}
			}
		}
		
		[TestMethod]
		[Description("Ensures that custom contracts are properly utilised.")]
		public void ContractorCustomContractTest() {
			// Create a Contractor that uses a custom contract
			var contractor = new Contractor(new TestContract());
			Assert.IsInstanceOfType(contractor.GetContract(typeof(ContractedClass)), typeof(TestContract));
		}

		[TestMethod]
		[Description("Ensures that custom contract factories are properly utilised.")]
		public void ContractorCustomFactoryTest() {
			// Create a Contractor that uses a custom contract factory to generate
			// contracts.
			var contractor = new Contractor(new TestContractFactory());
			Assert.IsInstanceOfType(contractor.GetContract(typeof(ContractedClass)), typeof(TestContract));
		}
	}
}
