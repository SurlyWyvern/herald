﻿using System;
using System.Collections.Generic;


using Microsoft.VisualStudio.TestTools.UnitTesting;

using SurlyWyvern.Herald;

namespace UnitTests {
	[TestClass]
	public class AliasBinderTest {
		[TestMethod]
		public void AliasBinderSimpleTest() {
			var binder = AliasBinder.DefaultInstance;

			// int
			var alias = binder.BindToAlias(typeof(int));
			var result = binder.BindToType(alias);
			Assert.IsNotNull(result);
			Assert.AreEqual(typeof(int), result);
			
			// From assembly and type name.
			var assemblyName = typeof(Serializer).Assembly.GetName().FullName;
			result = binder.BindToType(assemblyName, typeof(Serializer).FullName);
			Assert.IsNotNull(result);
			Assert.AreEqual(typeof(Serializer), result);
		}

		[TestMethod]
		[Description("Ensures that custom aliases are used.")]
		public void AliasBinderAliasTest() {
			var binder = new AliasBinder(new KeyValuePair<string, Type>("custom_alias", typeof(Serializer)));
			Assert.AreEqual("custom_alias", binder.BindToAlias(typeof(Serializer)));
			Assert.AreEqual(typeof(Serializer), binder.BindToType("custom_alias"));
		}

		[TestMethod]
		[Description("Ensures that the UseDefaults property works. If true, builtin types use default aliases.")]
		public void AliasBinderDefaultsTest() {
			var binder = AliasBinder.DefaultInstance;
			var alias = binder.BindToAlias(typeof(int));
			Assert.AreEqual("int", alias);

			binder = new AliasBinder(true, false);
			alias = binder.BindToAlias(typeof(int));
			Assert.AreEqual(alias, typeof(int).AssemblyQualifiedName);
		}

		[TestMethod]
		[Description("Ensures that the Exact property works correctly. If false, aliases must not contain version, culture, etc. data.")]
		public void AliasBinderExactTest() {
			var binder = new AliasBinder();
			var alias = binder.BindToAlias(typeof(Serializer));
			Assert.IsTrue(alias.Contains("Version"));
			Assert.IsTrue(alias.Contains("Culture"));
			Assert.IsTrue(alias.Contains("PublicKeyToken"));

			binder = new AliasBinder(false, true);
			alias = binder.BindToAlias(typeof(Serializer));
			Assert.IsFalse(alias.Contains("Version"));
			Assert.IsFalse(alias.Contains("Culture"));
			Assert.IsFalse(alias.Contains("PublicKeyToken"));
		}
	}
}
