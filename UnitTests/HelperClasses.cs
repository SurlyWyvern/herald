﻿using System;

using SurlyWyvern.Herald;

namespace UnitTests {

	public enum Color {
		Red,
		Green,
		Blue,
		Orangey_Purple,
	}

	public class ContractedClass : IEquatable<ContractedClass> {
		public int A_Property {
			get;
			set;
		}

		public float a_Field;

		private int Private_Property {
			get;
			set;
		}

		private float private_Field;
		
		public virtual bool Equals(ContractedClass other) {
			return other != null &&
				A_Property == other.A_Property &&
				a_Field == other.a_Field &&
				Private_Property == other.Private_Property &&
				private_Field == other.private_Field;
		}

		public override string ToString() {
			return "ContractedClass( " + A_Property + " | " + a_Field + ")";
		}

		public ContractedClass(int p, float f) {
			A_Property = p;
			a_Field = f;
			Private_Property = p;
			private_Field = f;
		}

	}

	public class SubContractedClass : ContractedClass, IEquatable<SubContractedClass> {
		public short OtherProperty {
			get;
			set;
		}

		public override bool Equals(ContractedClass other) {
			return Equals(other as SubContractedClass);
		}

		public bool Equals(SubContractedClass other) {
			return other != null && A_Property == other.A_Property && a_Field == other.a_Field && OtherProperty == other.OtherProperty;
		}
		
		public SubContractedClass(int p, float f, short s) : base(p, f) {
			OtherProperty = s;
		}

		public override string ToString() {
			return "SubContractedClass( " + A_Property + " | " + a_Field + " | " + OtherProperty + " )";
		}
	}

	public class SelfContractClass : ISelfContract, IEquatable<SelfContractClass> {
		public long Length {
			get;
			set;
		}

		// This field would be serialized by default.
		public ulong offset;

		public SelfContractClass(long l, ulong o) {
			Length = l;
			offset = o;
		}

		public void Read(Deserializer deserializer) {
			Length = deserializer.ReadInt64();
		}

		public void Write(Serializer serializer) {
			serializer.Serialize(Length);
		}

		public bool Equals(SelfContractClass other) {
			return other != null && Length == other.Length && offset == other.offset;
		}
	}

	public struct ContractedStruct : IEquatable<ContractedStruct> {
		public short x;

		public short Y {
			get;
			set;
		}

		public bool Equals(ContractedStruct other) {
			return x == other.x && Y == other.Y;
		}

		public ContractedStruct(short x, short y) {
			this.x = x;
			this.Y = y;
		}

		public override string ToString() {
			return "ContractedStruct(" + x + ", " + Y + ")";
		}
	}

	public struct SelfContractStruct : ISelfContract, IEquatable<SelfContractStruct> {
		public byte First {
			get;
			set;
		}

		public byte second;

		public bool Equals(SelfContractStruct other) {
			return First == other.First && second == other.second;
		}

		public void Write(Serializer serializer) {
			serializer.Serialize(second);
		}

		public void Read(Deserializer deserializer) {
			second = deserializer.ReadByte();
		}

		public SelfContractStruct(byte f, byte s) {
			First = f;
			second = s;
		}
	}
	
	public class SelfReferenceClass {
		public SelfReferenceClass other;
	}

	/// <summary>
	/// Contains fields and properties of all variations; structs, classes,
	/// nullables, subclasses etc. Used for testing contract generation.
	/// </summary>
	public class ComplexClass : IEquatable<ComplexClass> {
		#region Properties

		public int? NullableIntP {
			get;
			set;
		}

		public double DoubleP {
			get;
			set;
		}

		public Guid? NullableGuidP {
			get;
			set;
		}

		public DateTime DateTimeP {
			get;
			set;
		}

		public ContractedStruct? NullableStructP {
			get;
			set;
		}

		public ContractedStruct StructP {
			get;
			set;
		}

		public ContractedClass ClassP {
			get;
			set;
		}

		public SubContractedClass SubClassP {
			get;
			set;
		}

		public string StringP {
			get;
			set;
		}

		public Type TypeP {
			get;
			set;
		}

		#endregion

		#region Fields

		public int? NullableInt;

		public double Double;

		public Guid? NullableGuid;

		public DateTime DateTime;

		public ContractedStruct? NullableStruct;

		public ContractedStruct Struct;

		public ContractedClass Class;

		public SubContractedClass SubClass;

		public string String;

		public Type Type;

		#endregion

		public bool Equals(ComplexClass other) {
			return other != null &&
				NullableIntP == other.NullableIntP &&
				DoubleP == other.DoubleP &&
				NullableGuidP == other.NullableGuidP &&
				DateTimeP == other.DateTimeP &&
				NullableStructP.Equals(other.NullableStructP) &&
				StructP.Equals(other.StructP) &&
				ClassP.Equals(other.ClassP) &&
				SubClassP.Equals(other.SubClassP) &&
				StringP == other.StringP &&
				TypeP == other.TypeP &&
				NullableInt.Equals(other.NullableInt) &&
				Double.Equals(other.Double) &&
				NullableGuid.Equals(other.NullableGuid) &&
				DateTime.Equals(other.DateTime) &&
				Class.Equals(other.Class) &&
				SubClass.Equals(other.SubClass) &&
				String == other.String &&
				Type == other.Type;
		}
		
		public override string ToString() {
			return string.Format("ComplexStruct {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} | {10} | {11} | {12} | {13} | {14} | {15} | {16} | {17} | {18} | {19} )",
				NullableIntP,
				DoubleP,
				NullableGuidP,
				DateTimeP,
				NullableStructP,
				StructP,
				ClassP,
				SubClassP,
				StringP,
				TypeP,
				NullableInt,
				Double,
				NullableGuid,
				DateTime,
				NullableStruct,
				Struct,
				Class,
				SubClass,
				String,
				Type);
		}

		public ComplexClass(int? n, double d, Guid? g, DateTime dt, ContractedStruct? ns, ContractedStruct s, ContractedClass c, SubContractedClass sc, string st, Type t) {
			NullableIntP = n;
			NullableInt = n;
			DoubleP = d;
			Double = d;
			NullableGuidP = g;
			NullableGuid = g;
			DateTimeP = dt;
			DateTime = dt;
			NullableStructP = ns;
			NullableStruct = ns;
			StructP = s;
			Struct = s;
			ClassP = c;
			Class = c;
			SubClassP = sc;
			SubClass = sc;
			StringP = st;
			String = st;
			TypeP = t;
			Type = t;
		}

	}

	/// <summary>
	/// Contains fields and properties of all variations; structs, classes,
	/// nullables, subclasses etc. Used for testing contract generation.
	/// </summary>
	public struct ComplexStruct : IEquatable<ComplexStruct> {
		#region Properties

		public int? NullableIntP {
			get;
			set;
		}

		public double DoubleP {
			get;
			set;
		}

		public Guid? NullableGuidP {
			get;
			set;
		}

		public DateTime DateTimeP {
			get;
			set;
		}

		public ContractedStruct? NullableStructP {
			get;
			set;
		}

		public ContractedStruct StructP {
			get;
			set;
		}

		public ContractedClass ClassP {
			get;
			set;
		}

		public SubContractedClass SubClassP {
			get;
			set;
		}

		public string StringP {
			get;
			set;
		}

		public Type TypeP {
			get;
			set;
		}

		#endregion

		#region Fields

		public int? NullableInt;

		public double Double;

		public Guid? NullableGuid;

		public DateTime DateTime;

		public ContractedStruct? NullableStruct;

		public ContractedStruct Struct;

		public ContractedClass Class;

		public SubContractedClass SubClass;

		public string String;

		public Type Type;

		#endregion

		public bool Equals(ComplexStruct other) {
			return NullableIntP == other.NullableIntP &&
				DoubleP == other.DoubleP &&
				NullableGuidP == other.NullableGuidP &&
				DateTimeP == other.DateTimeP &&
				NullableStructP.Equals(other.NullableStructP) &&
				StructP.Equals(other.StructP) &&
				ClassP.Equals(other.ClassP) &&
				SubClassP.Equals(other.SubClassP) &&
				StringP == other.StringP &&
				TypeP == other.TypeP &&
				NullableInt.Equals(other.NullableInt) &&
				Double.Equals(other.Double) &&
				NullableGuid.Equals(other.NullableGuid) &&
				DateTime.Equals(other.DateTime) &&
				Class.Equals(other.Class) &&
				SubClass.Equals(other.SubClass) &&
				String == other.String &&
				Type == other.Type;
		}
		
		public ComplexStruct(int? n, double d, Guid? g, DateTime dt, ContractedStruct? ns, ContractedStruct s, ContractedClass c, SubContractedClass sc, string st, Type t) {
			NullableIntP = n;
			NullableInt = n;
			DoubleP = d;
			Double = d;
			NullableGuidP = g;
			NullableGuid = g;
			DateTimeP = dt;
			DateTime = dt;
			NullableStructP = ns;
			NullableStruct = ns;
			StructP = s;
			Struct = s;
			ClassP = c;
			Class = c;
			SubClassP = sc;
			SubClass = sc;
			StringP = st;
			String = st;
			TypeP = t;
			Type = t;
		}

		public override string ToString() {
			return string.Format("ComplexStruct {0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} | {10} | {11} | {12} | {13} | {14} | {15} | {16} | {17} | {18} | {19} )",
				NullableIntP,
				DoubleP,
				NullableGuidP,
				DateTimeP,
				NullableStructP,
				StructP,
				ClassP,
				SubClassP,
				StringP,
				TypeP,
				NullableInt,
				Double,
				NullableGuid,
				DateTime,
				NullableStruct,
				Struct,
				Class,
				SubClass,
				String,
				Type);
		}
	}

	#region Contracts

	public class TestContract : IContract {
		public bool CanProcess(Type type) {
			return type == typeof(ContractedClass);
		}

		public object Read(Deserializer deserializer, Type type = null, object existingValue = null) {
			var instance = Deserializer.CreateInstance<ContractedClass>();
			instance.A_Property = deserializer.ReadInt32();
			instance.a_Field = deserializer.ReadSingle();
			return instance;
		}

		public void Write(Serializer serializer, object value) {
			var instance = (ContractedClass)value;
			serializer.Serialize(-1);
			serializer.Serialize(2.3f);
		}
	}

	public class TestContractFactory : IContractFactory {
		public bool CanCreateFor(Type t) {
			return t == typeof(ContractedClass);
		}

		public IContract CreateFor(Type t) {
			return new TestContract();
		}
	}


	#endregion
}
